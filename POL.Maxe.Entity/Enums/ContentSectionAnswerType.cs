﻿namespace POL.Maxe.Entity.Enums
{
    public enum ContentSectionAnswerType
    {
        QUIZ = 1, CODE, SVG, FILL_BLANK, TEXT, MATH
    }

}
