﻿namespace POL.Maxe.Entity.Enums
{
    public enum DifficultyLevel
    {
        Easier, Easy, Normal, Intermediate, Hard, Expert
    }
}
