﻿namespace POL.Maxe.Entity.Enums
{
    public enum AnswerType
    {
        QUIZ = 1, CODE, SVG, LIST_DND, LIST_TEXT, FILL_BLANK, TEXT, MATH
    }

}
