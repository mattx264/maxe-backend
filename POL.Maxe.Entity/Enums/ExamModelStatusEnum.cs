﻿namespace POL.Maxe.Entity.Enums
{
    public enum ExamModelStatusEnum
    {
        draft = 1,
        review, 
        published,
        unpublished
    }
}
