﻿namespace POL.Maxe.Entity.Enums
{
    public enum ContentSectionQuestionType
    {
        TEXT,
        SVG,
        CODE,
        MATH,
        IMAGE,
        VIDEO,
        SOUND,    
        FILL_BLANK
    }
    
}
