﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.Entity.User;
using System;
using System.Collections.Generic;

namespace POL.Maxe.Entity.ExamModels
{
    public class AnswerModel : IEntity
    {
        public int Id { get; set; }
        public AnswerType AnswerType { get; set; }
        public virtual IList<ContentSectionAnswer> WrongAnswers { get; set; }
        public DifficultyLevel DifficultyLevel { get; set; }
        public int QuestionModelId { get; set; }
        public virtual QuestionModel QuestionModel { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
        
    }
   
}
