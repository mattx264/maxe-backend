﻿using POL.Maxe.Entity.Interfaces;
using System;
using System.Collections.Generic;

namespace POL.Maxe.Entity.ExamModels
{
    public class QuestionModel : IEntity
    {
        public int Id { get; set; }
        public virtual IList<ContentSectionQuestion> QuestionSections { get; set; }
        public virtual IList<AnswerModel> AnswerList { get; set; }
        public virtual IList<ContentSectionCorrectAnswer> ContentSectionCorrectAnswers { get; set; }
        public string Note { get; set; }
        public virtual ExamModel ExamModel { get; set; }
        public int ExamModelId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}