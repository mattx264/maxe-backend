﻿using POL.Maxe.Entity.Enums;

namespace POL.Maxe.Entity.ExamModels.Interfaces
{
    public interface IContentSection
    {
        public string Value { get; set; }
        public ContentSectionAnswerType Type { get; set; }
    }
}
