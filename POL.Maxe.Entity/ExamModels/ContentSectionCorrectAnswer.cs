﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels.Interfaces;
using POL.Maxe.Entity.Interfaces;
using System;

namespace POL.Maxe.Entity.ExamModels
{
    public class ContentSectionCorrectAnswer : IEntity, IContentSection
    {
        public int Id { get; set; }
        public virtual QuestionModel QuestionModel { get; set; }
        public int QuestionModelId { get; set; }
        public string Value { get; set; }
        public ContentSectionAnswerType Type { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}
