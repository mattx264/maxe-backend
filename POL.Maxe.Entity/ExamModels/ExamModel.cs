﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace POL.Maxe.Entity.ExamModels
{
    /// <summary>
    /// Exam Model is 'folder' for question. It's grouping questions by category
    /// </summary>
    public class ExamModel : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryExamId { get; set; }
        public virtual CategoryExam CategoryExam { get; set; }
        [Required]
        public string Description { get; set; }
        public virtual List<QuestionModel> QuestionModels { get; set; }
        public string Tags { get; set; } //this should be reference to table
        public int FileModelId { get; set; }
        public virtual FileModel FileModel { get; set; }
        public int UserModelId { get; set; }
        public virtual UserModel UserModel { get; set; }
        public string Language { get; set; }
        public ExamModelStatusEnum Status { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}

