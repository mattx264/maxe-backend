﻿using POL.Maxe.Entity.ExamModels;
using System;

namespace POL.Maxe.Entity.Interfaces
{
    public class RunQuestionModel : IEntity
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public virtual QuestionModel Question { get; set; }
        public bool IsQuestionPlayed { get; set; }
        public int RememberQuestionModel { get; set; }
        public string UserAnswer { get; set; }
        //public int AnswerIndex { get; set; }  -- dont know why index is needs here
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}
