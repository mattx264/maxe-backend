﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace POL.Maxe.Entity.ExamRunModels
{
    /// <summary>
    /// ExamRunModel keep track of users exams
    /// </summary>
    public class ExamRunModel : IEntity
    {
        [Key]
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Language { get; set; }
        public int CurrentQuestionIndex { get; set; }
        public virtual UserModel UserModel { get; set; }
        public int? QuestionRepeat { get; set; }
        public virtual IList<RunQuestionModel> RunQuestionModels { get; set; }
        public ExamTypeEnum ExamType { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}
