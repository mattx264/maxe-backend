﻿using POL.Maxe.Entity.Interfaces;
using System;

namespace POL.Maxe.Entity.User
{
    public class FileModel : IEntity
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsActive { get; set; }
        public int UserModelId { get; set; }
        public virtual UserModel UserModel { get; set; }
        public string Extension { get; set; }
        public string GuidName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }       
    }
}
