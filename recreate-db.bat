cd POL.Maxe.DataAccess
iisreset
del /f /q Migrations
dotnet ef --startup-project ../POL.Maxe.WebService database drop -f
dotnet ef --startup-project ../POL.Maxe.WebService migrations add Init -c MaxeDbContext
dotnet ef --startup-project ../POL.Maxe.WebService database update Init -c MaxeDbContext 
cd..