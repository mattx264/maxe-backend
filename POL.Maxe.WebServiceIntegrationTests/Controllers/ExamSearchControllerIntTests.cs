﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceIntegrationTests.Controllers
{
    [TestFixture]

    public class ExamSearchControllerIntTests
    {
        private ExamSearchController _controller;

        [SetUp]
        public void Setup()
        {
            var serviceProvider = new Startup().GetSerice();
            _controller = serviceProvider.GetService<ExamSearchController>();
            _controller.ControllerContext.HttpContext =new  DefaultHttpContext();
        }
        [Test]
        public void Get_Can()
        {
            var result = _controller.GetAll();
            Assert.IsNotNull(result);
        }
    }    
}
