﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceIntegrationTests.Controllers
{
    [TestFixture]
    class ExamRunControllerIntTests
    {
        private ExamRunController _controller;
        // private ServiceProvider serviceProvider { get; set; }


        [SetUp]
        public void Setup()
        {
            var serviceProvider = new Startup().GetSerice();
            _controller = serviceProvider.GetService<ExamRunController>();

        }

        [Test]
        public async Task StartAsync()
        {
            var result = await _controller.StartAsync(new SaveExamRunViewModel()
            {
                ExamIds = new int[] { 1 },
                QuestionLimit = null,
                QuestionRepeat = null,
                RandomizeQuestions = false

            });

            var resultFromGet = _controller.Get(result);
            Assert.AreEqual(resultFromGet.Guid, new Guid(result.ToString()));
        }


    }
}
