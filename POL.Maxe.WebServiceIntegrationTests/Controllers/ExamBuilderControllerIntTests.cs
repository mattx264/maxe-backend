﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using POL.Maxe.WebService.Controllers.Builder;
using System.IO;
using System.Threading;

namespace POL.Maxe.WebServiceIntegrationTests.Controllers
{
    [TestFixture]
    class ExamBuilderControllerIntTests
    {
        private FileExamBuilderController _controller;

        [SetUp]
        public void Setup()
        {
            var serviceProvider = new Startup().GetSerice();
            _controller = serviceProvider.GetService<FileExamBuilderController>();

        }
        [Test]
        public void FileUpload()
        {
            //Arrange
            var fileMock = new Mock<IFormFile>();
            //Setup mock file using a memory stream
            var content = @"question,TEXT,Name design patters category,,";
            var fileName = "test.csv";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);

            var file = fileMock.Object;

            //Act
            var result = _controller.UploadExamBulkFileAsync(file, CancellationToken.None);

            //Assert
            //Assert.IsInstanceOfType(result, typeof(IActionResult));
        }
    }
}
