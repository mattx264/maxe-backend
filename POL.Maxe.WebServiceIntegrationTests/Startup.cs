﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.Controllers.Builder;
using POL.Maxe.WebService.Helpers;
using POL.Maxe.WebService.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceIntegrationTests
{
    public class Startup
    {
        public ServiceProvider GetSerice()
        {
            var services = new ServiceCollection();

            services.AddDbContext<MaxeDbContext>(options =>
               //  options.UseInMemoryDatabase("inmemory")
               options.UseSqlite(CreateInMemoryDatabase())
            );

            services.AddTransient<IUserModelRepository, UserModelRepository>();

            services.AddTransient<IExamRunModelRepository, ExamRunRepository>();
            services.AddTransient<IExamModelRepository, ExamModelRepository>();
            services.AddTransient<IExamModelRepository, ExamModelRepository>();
            services.AddTransient<ICategoryExamRepository, CategoryExamRepository>();
            services.AddTransient<IAuthenticationHelper, AuthenticationHelper>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ExamRunController>();
            services.AddTransient<ExamSearchController>();
            services.AddTransient<FileExamBuilderController>();


            var serviceProvider = services.BuildServiceProvider();
            var context = serviceProvider.GetRequiredService<MaxeDbContext>();
            context.Database.EnsureCreated();
            return serviceProvider;

            //var options = new DbContextOptionsBuilder<MaxeDbContext>()
            //   .UseInMemoryDatabase(databaseName: "Test")
            //   .Options;
            //using (var context = new MaxeDbContext(options))
            //{
            //    var examRunModelRepository = new ExamRunRepository(context);
            //    var examModelRepository = new ExamModelRepository(context);
            //    var unitOfWork = new UnitOfWork(context);


            //    _controller = new ExamRunController(
            //        examRunModelRepository
            //        , examModelRepository
            //        , unitOfWork);
            //}
        }
        private DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }
    }
}
