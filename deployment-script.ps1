Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'
$PasswordServer=''
function Show-Menu {
    param (
        [string]$Title = 'My Menu'
    )
    Clear-Host
    Write-Host "================ $Title ================"
    
    Write-Host "1: Press '1' for build and deploy."
    Write-Host "2: Press '2' for deploy."
    Write-Host "3: Press '3' for build."
    Write-Host "4: Press '4' for deploy Frontend."
    Write-Host "5: Press '5' for deploy Backend."
    Write-Host "Q: Press 'Q' to quit."
}
function Build {
    "start " + (get-date).tostring()
   
    $startLocation = Get-Location
    Set-Location -Path .\POL.Maxe.WebService
    dotnet publish --configuration Release

    Set-Location -Path ..\..\maxe-frontend
    npm run build
    Set-Location $startLocation 
}
function Deploy {
    $startLocation = Get-Location
	if($PasswordServer -eq ''){
		$PasswordServer = Read-Host -Prompt 'Password'
	}
    Add-Type -Path "C:\Program Files (x86)\WinSCP\WinSCPnet.dll"
  
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.ParseUrl("ftp://mattx2642-001:" + $PasswordServer + "@win5047.site4now.net")
    $session = New-Object WinSCP.Session
    $session.Open($sessionOptions)
    $pathDotnet = (Get-Location).tostring() + "\POL.Maxe.WebService\bin\Release\net6.0\publish\*"   
    Write-Output $pathDotnet
    $session.PutFiles($pathDotnet, "/maxe/").Check()
    Set-Location -Path ..\maxe-frontend\dist\
    $pathAngular = (Get-Location).tostring() + "\maxe\*"   
    Write-Output $pathAngular
    $session.PutFiles($pathAngular, "/maxe/wwwroot/").Check()
    $session.Dispose()
    Set-Location $startLocation 
    "Done " + (get-date).tostring()
}
function DeployFrontEnd {
	 try
    {
    $startLocation = Get-Location
    if($PasswordServer -eq ''){
		$PasswordServer = Read-Host -Prompt 'Password'
	}
    Add-Type -Path "C:\Program Files (x86)\WinSCP\WinSCPnet.dll"  
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.ParseUrl("ftp://mattx2642-001:" + $PasswordServer + "@win5047.site4now.net")
    $session = New-Object WinSCP.Session
    $session.Open($sessionOptions)    
    Set-Location -Path ..\maxe-frontend\dist\
    $pathAngular = (Get-Location).tostring() + "\maxe\*"   
    Write-Output $pathAngular
	
	$transferOptions = New-Object WinSCP.TransferOptions
    $transferOptions.TransferMode =  [WinSCP.TransferMode]::Binary
	
    $transferResult = $session.PutFiles($pathAngular, "/maxe/wwwroot/", $False, $transferOptions)
	  $transferResult.Check()
 
        # Print results
        foreach ($transfer in $transferResult.Transfers)
        {
            Write-Host "Upload of $($transfer.FileName) succeeded"
        }
		}
    finally
    {
$session.Dispose()
    Set-Location $startLocation 
	
    "Done " + (get-date).tostring()
    }
 
    exit 0
    
}
function DeployBackEnd {
    $startLocation = Get-Location
    if($PasswordServer -eq ''){
		$PasswordServer = Read-Host -Prompt 'Password'
	}
    Add-Type -Path "C:\Program Files (x86)\WinSCP\WinSCPnet.dll"  
    $sessionOptions = New-Object WinSCP.SessionOptions
    $sessionOptions.ParseUrl("ftp://mattx2642-001:" + $PasswordServer + "@win5047.site4now.net")
    $session = New-Object WinSCP.Session
    $session.Open($sessionOptions)    
    $pathDotnet = (Get-Location).tostring() + "\POL.Maxe.WebService\bin\Release\net6.0\publish\*"   
    Write-Output $pathDotnet
    $session.PutFiles($pathDotnet, "/maxe/").Check()
    $session.Dispose()
    Set-Location $startLocation 
    "Done " + (get-date).tostring()
}
do {
    Show-Menu
    $selection = Read-Host "Please make a selection"
    switch ($selection) {
        '1' {
			$PasswordServer = Read-Host -Prompt 'Password'
            Build
            Deploy
        } '2' {
            Deploy
        } '3' {
            Build
        } '4' {
            DeployFrontEnd
        } '5' {
            DeployBackEnd
        } 
    }
    pause
}
until ($selection -eq 'q')