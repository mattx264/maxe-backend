﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POL.Maxe.DataAccess.Migrations
{
    public partial class AddExamStaus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "ExamModel",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Staus",
                table: "ExamModel");
        }
    }
}
