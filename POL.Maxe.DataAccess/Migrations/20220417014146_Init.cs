﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace POL.Maxe.DataAccess.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryExam",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParentCategoryExamId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryExam", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryExam_CategoryExam_ParentCategoryExamId",
                        column: x => x.ParentCategoryExamId,
                        principalTable: "CategoryExam",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    Avator = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExamRunModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Language = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CurrentQuestionIndex = table.Column<int>(type: "int", nullable: false),
                    UserModelId = table.Column<int>(type: "int", nullable: true),
                    QuestionRepeat = table.Column<int>(type: "int", nullable: true),
                    ExamType = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamRunModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamRunModel_UserModel_UserModelId",
                        column: x => x.UserModelId,
                        principalTable: "UserModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FileModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FilePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsPrivate = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    UserModelId = table.Column<int>(type: "int", nullable: false),
                    Extension = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GuidName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileModel_UserModel_UserModelId",
                        column: x => x.UserModelId,
                        principalTable: "UserModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CategoryExamId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Tags = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileModelId = table.Column<int>(type: "int", nullable: false),
                    UserModelId = table.Column<int>(type: "int", nullable: false),
                    Language = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamModel_CategoryExam_CategoryExamId",
                        column: x => x.CategoryExamId,
                        principalTable: "CategoryExam",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamModel_FileModel_FileModelId",
                        column: x => x.FileModelId,
                        principalTable: "FileModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamModel_UserModel_UserModelId",
                        column: x => x.UserModelId,
                        principalTable: "UserModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuestionModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExamModelId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionModel_ExamModel_ExamModelId",
                        column: x => x.ExamModelId,
                        principalTable: "ExamModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnswerModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AnswerType = table.Column<int>(type: "int", nullable: false),
                    DifficultyLevel = table.Column<int>(type: "int", nullable: false),
                    QuestionModelId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnswerModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnswerModel_QuestionModel_QuestionModelId",
                        column: x => x.QuestionModelId,
                        principalTable: "QuestionModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentSectionCorrectAnswer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuestionModelId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentSectionCorrectAnswer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentSectionCorrectAnswer_QuestionModel_QuestionModelId",
                        column: x => x.QuestionModelId,
                        principalTable: "QuestionModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentSectionQuestion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    QuestionModelId = table.Column<int>(type: "int", nullable: false),
                    FileModelId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentSectionQuestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentSectionQuestion_FileModel_FileModelId",
                        column: x => x.FileModelId,
                        principalTable: "FileModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContentSectionQuestion_QuestionModel_QuestionModelId",
                        column: x => x.QuestionModelId,
                        principalTable: "QuestionModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RunQuestionModel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuestionId = table.Column<int>(type: "int", nullable: false),
                    IsQuestionPlayed = table.Column<bool>(type: "bit", nullable: false),
                    RememberQuestionModel = table.Column<int>(type: "int", nullable: false),
                    UserAnswer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    ExamRunModelId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RunQuestionModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RunQuestionModel_ExamRunModel_ExamRunModelId",
                        column: x => x.ExamRunModelId,
                        principalTable: "ExamRunModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RunQuestionModel_QuestionModel_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "QuestionModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContentSectionAnswer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AnswerModelId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    ModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true, defaultValue: "system"),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified)),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentSectionAnswer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentSectionAnswer_AnswerModel_AnswerModelId",
                        column: x => x.AnswerModelId,
                        principalTable: "AnswerModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "CategoryExam",
                columns: new[] { "Id", "Name", "ParentCategoryExamId" },
                values: new object[,]
                {
                    { 1, "General", null },
                    { 2, "Politics", null },
                    { 3, "Art", null },
                    { 4, "Music", 4 },
                    { 5, "Philosophy, Religion, & Theology", null },
                    { 6, "Biological", null },
                    { 7, "Military", null },
                    { 8, "Physical Sciences", null },
                    { 9, "Literature", null },
                    { 10, "Language", null },
                    { 11, "Engineering", null },
                    { 12, "Automobile", 12 },
                    { 13, "Law", null },
                    { 14, "Mathematics", null },
                    { 15, "Geographic", null },
                    { 16, "History", null },
                    { 17, "Chemistry", null },
                    { 18, "IT", null }
                });

            migrationBuilder.InsertData(
                table: "UserModel",
                columns: new[] { "Id", "Avator", "Email", "FirstName", "Guid", "LastName", "Password", "PhoneNumber" },
                values: new object[] { 1, "Default1", "test@test.com", "Joe", new Guid("c8366a69-ce09-4591-9399-64e831e991e4"), "Test", "5lWUcAhyHCV2rTZqqyE8JIMZJjjAwlwRMrq5jxH+KQY=", "5555555555" });

            migrationBuilder.InsertData(
                table: "CategoryExam",
                columns: new[] { "Id", "Name", "ParentCategoryExamId" },
                values: new object[,]
                {
                    { 19, "Programing", 18 },
                    { 35, "Network", 18 }
                });

            migrationBuilder.InsertData(
                table: "FileModel",
                columns: new[] { "Id", "Extension", "FileName", "FilePath", "GuidName", "IsPrivate", "UserModelId" },
                values: new object[,]
                {
                    { 1, "jpg", "abc.jpg", "demo/abc.jpg", "abc", false, 1 },
                    { 2, "jpg", "code.jpg", "demo/code.jpg", "code", false, 1 }
                });

            migrationBuilder.InsertData(
                table: "CategoryExam",
                columns: new[] { "Id", "Name", "ParentCategoryExamId" },
                values: new object[,]
                {
                    { 20, "Assebly", 19 },
                    { 21, "C", 19 },
                    { 22, "C++", 19 },
                    { 23, "C#", 19 },
                    { 24, "CSS", 19 },
                    { 25, "GO", 19 },
                    { 26, "Html", 19 },
                    { 27, "Java", 19 },
                    { 28, "JavaScript", 19 },
                    { 29, "Katlin", 19 },
                    { 30, "PHP", 19 },
                    { 31, "Python", 19 },
                    { 32, "R", 19 },
                    { 33, "Swift", 19 },
                    { 34, "TypeScript", 19 }
                });

            migrationBuilder.InsertData(
                table: "ExamModel",
                columns: new[] { "Id", "CategoryExamId", "Description", "FileModelId", "Language", "Tags", "Title", "UserModelId" },
                values: new object[] { 1, 1, "abcd", 1, "en", "", "abcd", 1 });

            migrationBuilder.InsertData(
                table: "QuestionModel",
                columns: new[] { "Id", "ExamModelId", "Note" },
                values: new object[] { 1, 1, null });

            migrationBuilder.InsertData(
                table: "AnswerModel",
                columns: new[] { "Id", "AnswerType", "DifficultyLevel", "QuestionModelId" },
                values: new object[,]
                {
                    { 1, 1, 2, 1 },
                    { 2, 7, 4, 1 }
                });

            migrationBuilder.InsertData(
                table: "ContentSectionCorrectAnswer",
                columns: new[] { "Id", "QuestionModelId", "Type", "Value" },
                values: new object[] { 1, 1, 5, "Earth" });

            migrationBuilder.InsertData(
                table: "ContentSectionQuestion",
                columns: new[] { "Id", "FileModelId", "QuestionModelId", "Type", "Value" },
                values: new object[] { 1, null, 1, 0, "On what planet we live on ?" });

            migrationBuilder.InsertData(
                table: "ContentSectionAnswer",
                columns: new[] { "Id", "AnswerModelId", "Type", "Value" },
                values: new object[] { 1, 1, 5, "Mars" });

            migrationBuilder.InsertData(
                table: "ContentSectionAnswer",
                columns: new[] { "Id", "AnswerModelId", "Type", "Value" },
                values: new object[] { 2, 1, 5, "Venus" });

            migrationBuilder.InsertData(
                table: "ContentSectionAnswer",
                columns: new[] { "Id", "AnswerModelId", "Type", "Value" },
                values: new object[] { 3, 1, 5, "Sun" });

            migrationBuilder.CreateIndex(
                name: "IX_AnswerModel_QuestionModelId",
                table: "AnswerModel",
                column: "QuestionModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryExam_ParentCategoryExamId",
                table: "CategoryExam",
                column: "ParentCategoryExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSectionAnswer_AnswerModelId",
                table: "ContentSectionAnswer",
                column: "AnswerModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSectionCorrectAnswer_QuestionModelId",
                table: "ContentSectionCorrectAnswer",
                column: "QuestionModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSectionQuestion_FileModelId",
                table: "ContentSectionQuestion",
                column: "FileModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ContentSectionQuestion_QuestionModelId",
                table: "ContentSectionQuestion",
                column: "QuestionModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamModel_CategoryExamId",
                table: "ExamModel",
                column: "CategoryExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamModel_FileModelId",
                table: "ExamModel",
                column: "FileModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamModel_UserModelId",
                table: "ExamModel",
                column: "UserModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamRunModel_UserModelId",
                table: "ExamRunModel",
                column: "UserModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FileModel_UserModelId",
                table: "FileModel",
                column: "UserModelId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionModel_ExamModelId",
                table: "QuestionModel",
                column: "ExamModelId");

            migrationBuilder.CreateIndex(
                name: "IX_RunQuestionModel_ExamRunModelId",
                table: "RunQuestionModel",
                column: "ExamRunModelId");

            migrationBuilder.CreateIndex(
                name: "IX_RunQuestionModel_QuestionId",
                table: "RunQuestionModel",
                column: "QuestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContentSectionAnswer");

            migrationBuilder.DropTable(
                name: "ContentSectionCorrectAnswer");

            migrationBuilder.DropTable(
                name: "ContentSectionQuestion");

            migrationBuilder.DropTable(
                name: "RunQuestionModel");

            migrationBuilder.DropTable(
                name: "AnswerModel");

            migrationBuilder.DropTable(
                name: "ExamRunModel");

            migrationBuilder.DropTable(
                name: "QuestionModel");

            migrationBuilder.DropTable(
                name: "ExamModel");

            migrationBuilder.DropTable(
                name: "CategoryExam");

            migrationBuilder.DropTable(
                name: "FileModel");

            migrationBuilder.DropTable(
                name: "UserModel");
        }
    }
}
