﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.Interfaces;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace POL.Maxe.DataAccess.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class, IEntity
    {

        protected MaxeDbContext context { get; set; }
        public RepositoryBase(MaxeDbContext context)
        {
            this.context = context;

        }
        public T FindById(int id)
        {
            return this.context.Set<T>().FirstOrDefault(x=>x.Id==id && x.IsActive);
        }

        public IQueryable<T> FindAll()
        {
            return this.context.Set<T>().Where(x => x.IsActive);
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.context.Set<T>().Where(x => x.IsActive).Where(expression);
        }

        public void Create(T entity)
        {
            this.context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            entity.IsActive = false;
            this.context.Set<T>().Update(entity);
        }
    }

}
