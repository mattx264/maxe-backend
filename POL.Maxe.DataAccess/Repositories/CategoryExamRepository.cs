﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;

namespace POL.Maxe.DataAccess.Repositories
{
    public class CategoryExamRepository : RepositoryBase<CategoryExam>, ICategoryExamRepository
    {
        public CategoryExamRepository(MaxeDbContext context) : base(context)
        {

        }
    }
}
