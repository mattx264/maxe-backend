﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.User;
using System;
using System.Linq;

namespace POL.Maxe.DataAccess.Repositories
{
    public class UserModelRepository : RepositoryBase<UserModel>, IUserModelRepository
    {
        public UserModelRepository(MaxeDbContext context) : base(context)
        {
        }
        public UserModel Login(string email, string password)
        {
            return context.UserModel.FirstOrDefault(x => x.Email == email && x.Password == password);
        }

        public UserModel GetByGuid(string guidString)
        {
            Guid guid = new Guid(guidString);
            return context.UserModel.FirstOrDefault(x => x.Guid == guid);
        }
    }
}
