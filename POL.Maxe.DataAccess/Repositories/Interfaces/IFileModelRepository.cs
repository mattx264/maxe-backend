﻿using POL.Maxe.Entity.User;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface IFileModelRepository : IRepositoryBase<FileModel>
    {
    }
}
