﻿using POL.Maxe.Entity.ExamRunModels;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface IExamRunModelRepository : IRepositoryBase<ExamRunModel>
    {
    }
}
