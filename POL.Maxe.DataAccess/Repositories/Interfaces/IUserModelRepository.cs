﻿
using POL.Maxe.Entity.User;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface IUserModelRepository : IRepositoryBase<UserModel>
    {
        public UserModel Login(string email, string password);

        public UserModel GetByGuid(string guidString);
    }
}
