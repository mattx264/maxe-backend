﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        T FindById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
