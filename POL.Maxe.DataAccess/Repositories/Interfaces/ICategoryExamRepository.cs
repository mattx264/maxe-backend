﻿using POL.Maxe.Entity.ExamModels;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface ICategoryExamRepository : IRepositoryBase<CategoryExam>
    {
    }
}
