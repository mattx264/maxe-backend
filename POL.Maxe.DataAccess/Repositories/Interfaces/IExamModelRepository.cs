﻿using POL.Maxe.Entity.ExamModels;
using System.Collections.Generic;

namespace POL.Maxe.DataAccess.Repositories.Interfaces
{
    public interface IExamModelRepository : IRepositoryBase<ExamModel>
    {
        IList<ExamModel> GetByUser(int userModelId);
        ExamModel FindByIdAllActive(int id);
    }
}
