﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;

namespace POL.Maxe.DataAccess.Repositories
{
    public class QuestionRepository : RepositoryBase<QuestionModel>, IQuestionRepository
    {
        public QuestionRepository(MaxeDbContext context) : base(context)
        {

        }
    }
}
