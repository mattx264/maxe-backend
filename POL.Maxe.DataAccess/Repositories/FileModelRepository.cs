﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.User;

namespace POL.Maxe.DataAccess.Repositories
{
    public class FileModelRepository : RepositoryBase<FileModel>, IFileModelRepository
    {
        public FileModelRepository(MaxeDbContext context) : base(context)
        {
        }
    }
}
