﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamRunModels;

namespace POL.Maxe.DataAccess.Repositories
{
    public class ExamRunRepository : RepositoryBase<ExamRunModel>, IExamRunModelRepository
    {
        public ExamRunRepository(MaxeDbContext context) : base(context)
        {

        }
    }
}
