﻿using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;
using System.Collections.Generic;
using System.Linq;

namespace POL.Maxe.DataAccess.Repositories
{
    public class ExamModelRepository : RepositoryBase<ExamModel>, IExamModelRepository
    {
        public ExamModelRepository(MaxeDbContext context) : base(context)
        {

        }
        public ExamModel FindByIdAllActive(int id)
        {
            var examModel = FindById(id);
            examModel.QuestionModels = examModel.QuestionModels.Where(x => x.IsActive).ToList();
            
            return examModel;
        }
        public IList<ExamModel> GetByUser(int userModelId)
        {
            return FindAll().Where(x => x.UserModelId == userModelId).ToList();
        }
    }
}
