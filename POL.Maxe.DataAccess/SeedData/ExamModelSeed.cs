﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels;
using System.Collections.Generic;

namespace POL.Maxe.DataAccess.SeedData
{
    public static class ExamModelSeed
    {
        public static ExamModel GetTestExamModel()
        {
            var examModel = GetExamModel();

            var questionModel = GetQuestionModel();

            questionModel.QuestionSections = new List<ContentSectionQuestion> { GetContentSectionQuestion() };
            questionModel.AnswerList = GetAnswerModel();
            questionModel.AnswerList[0].WrongAnswers = GetContentSectionAnswers();
            questionModel.ContentSectionCorrectAnswers = new List<ContentSectionCorrectAnswer> { GetContentSectionCorrectAnswer() };
        
            examModel.QuestionModels = new List<QuestionModel>() { questionModel };
            return examModel;
        }
        public static ExamModel GetExamModel()
        {
            return new ExamModel
            {
                Id = 1,
                Description = "abcd",
                Title = "abcd",
                CategoryExamId = 1,
                Language = "en",
                FileModelId = 1,
                Status = ExamModelStatusEnum.published,
                Tags = "",
                UserModelId = 1

            };
        }
        public static ContentSectionCorrectAnswer GetContentSectionCorrectAnswer()
        {
            return new ContentSectionCorrectAnswer()
            {
                Id = 1,
                QuestionModelId = 1,
                Value = "Earth",
                Type = ContentSectionAnswerType.TEXT
            };
        }
        public static List<ContentSectionAnswer> GetContentSectionAnswers()
        {
            return new List<ContentSectionAnswer>{ new ContentSectionAnswer()
            {
                Id = 1,
                    AnswerModelId = 1,
                    Value = "Mars",
                    Type = ContentSectionAnswerType.TEXT
                },
                new ContentSectionAnswer()
            {
                Id = 2,
                    AnswerModelId = 1,
                    Value = "Venus",
                    Type = ContentSectionAnswerType.TEXT
                }
                   , new ContentSectionAnswer()
            {
                Id = 3,
                AnswerModelId = 1,
                Value = "Sun",
                Type = ContentSectionAnswerType.TEXT
            }
        };
        }
        public static QuestionModel GetQuestionModel()
        {
            return new QuestionModel
            {
                Id = 1,
                ExamModelId = 1,

            };
        }
        public static List<AnswerModel> GetAnswerModel()
        {
            return new List<AnswerModel> { new AnswerModel()
            {
                Id = 1,
                AnswerType = AnswerType.QUIZ,
                DifficultyLevel = DifficultyLevel.Normal,
                QuestionModelId = 1
            },new AnswerModel()
            {
                Id = 2,
                AnswerType = AnswerType.TEXT,
                DifficultyLevel = DifficultyLevel.Hard,
                QuestionModelId = 1

            }};
        }
        public static ContentSectionQuestion GetContentSectionQuestion()
        {
            return new ContentSectionQuestion()
            {
                Id = 1,
                Type = ContentSectionQuestionType.TEXT,
                Value = "On what planet we live on ?",
                QuestionModelId = 1
            };
        }
    }
}
