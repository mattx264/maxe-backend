﻿using POL.Maxe.DataAccess.DatabaseContext;
using System.Threading.Tasks;

namespace POL.Maxe.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private MaxeDbContext _context;

        public UnitOfWork(MaxeDbContext context)
        {
            _context = context;
        }
        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(); ;
        }
    }

}
