﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using POL.Maxe.DataAccess.SeedData;
using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.Entity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace POL.Maxe.DataAccess.DatabaseContext
{
    public partial class MaxeDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region only active 
            Expression<Func<IEntity, bool>> expression = x => x.IsActive;
            var entities = modelBuilder.Model
                .GetEntityTypes()
                .Where(e => e.ClrType.GetInterface(typeof(IEntity).Name) != null)
                .Select(e => e.ClrType);
            foreach (var entity in entities)
            {
                var newParam = Expression.Parameter(entity);
                var newbody = ReplacingExpressionVisitor.Replace(expression.Parameters.Single(), newParam, expression.Body);
                modelBuilder.Entity(entity).HasQueryFilter(Expression.Lambda(newbody, newParam));
            }
            #endregion
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            }
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {               

                // foreach (var property in entity.GetProperties().Where(p => p.ClrType == typeof(string)))

                foreach (var property in entity.GetProperties().Where(p => p.Name == "IsActive"))
                {
                    property.SetDefaultValue(true);
                }
                foreach (var property in entity.GetProperties().Where(p => p.Name == "CreatedBy"))
                {
                    property.SetDefaultValue("system");
                }
                foreach (var property in entity.GetProperties().Where(p => p.Name == "ModifiedBy"))
                {
                    property.SetDefaultValue("system");
                }
                foreach (var property in entity.GetProperties().Where(p => p.Name == "DateAdded"))
                {
                    property.SetDefaultValue(new DateTime(2020, 1, 1));
                }
                foreach (var property in entity.GetProperties().Where(p => p.Name == "DateModified"))
                {
                    property.SetDefaultValue(new DateTime(2020, 1, 1));
                }
            }

          
            CategoryExamCreate(modelBuilder);
            DemoData(modelBuilder);

            base.OnModelCreating(modelBuilder);

        }
       
        private void CategoryExamCreate(ModelBuilder modelBuilder)
        {
            int categoryExamId = 1;
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "General"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Politics"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Art"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Music",
                ParentCategoryExamId = categoryExamId - 1
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Philosophy, Religion, & Theology"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Biological"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Military"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Physical Sciences"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Literature"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Language"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Engineering"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Automobile",
                ParentCategoryExamId = categoryExamId - 1
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Law"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Mathematics"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Geographic"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "History"
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Chemistry"
            });
            int ITid = categoryExamId++;
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = ITid,
                Name = "IT"
            });
            int Programingid = categoryExamId++;

            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = Programingid,
                Name = "Programing",
                ParentCategoryExamId = ITid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Assebly",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "C",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "C++",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "C#",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "CSS",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "GO",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Html",
                ParentCategoryExamId = Programingid
            });
                        modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Java",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "JavaScript",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Katlin",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "PHP",
                ParentCategoryExamId = Programingid
            }); modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Python",
                ParentCategoryExamId = Programingid
            }); modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "R",
                ParentCategoryExamId = Programingid
            }); modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Swift",
                ParentCategoryExamId = Programingid
            }); modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "TypeScript",
                ParentCategoryExamId = Programingid
            });
            modelBuilder.Entity<CategoryExam>().HasData(new CategoryExam
            {
                Id = categoryExamId++,
                Name = "Network",
                ParentCategoryExamId = ITid
            });
        }

        private void DemoData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>().HasData(new UserModel
            {
                Id = 1,
                Guid = Guid.Parse("c8366a69-ce09-4591-9399-64e831e991e4"),
                FirstName = "Joe",
                LastName = "Test",
                PhoneNumber = "5555555555",
                Email = "test@test.com",
                Password = PasswordHash("test123"),
                Avator = "Default1"
            });
            modelBuilder.Entity<FileModel>().HasData(new FileModel()
            {
                Id = 1,
                UserModelId = 1,
                Extension = "jpg",
                GuidName = "abc",
                FileName = "abc.jpg",
                FilePath = "demo/abc.jpg"
            });
            modelBuilder.Entity<FileModel>().HasData(new FileModel()
            {
                Id = 2,
                UserModelId = 1,
                Extension = "jpg",
                GuidName = "code",
                FileName = "code.jpg",
                FilePath = "demo/code.jpg"
            });
            modelBuilder.Entity<ExamModel>().HasData(ExamModelSeed.GetExamModel());
            modelBuilder.Entity<ContentSectionCorrectAnswer>().HasData(
               ExamModelSeed.GetContentSectionCorrectAnswer());
            modelBuilder.Entity<ContentSectionAnswer>().HasData(
           ExamModelSeed.GetContentSectionAnswers()
               );

            modelBuilder.Entity<QuestionModel>().HasData(ExamModelSeed.GetQuestionModel()
           );

            modelBuilder.Entity<AnswerModel>().HasData(ExamModelSeed.GetAnswerModel());
            modelBuilder.Entity<ContentSectionQuestion>().HasData(ExamModelSeed.GetContentSectionQuestion());
        }

        private string PasswordHash(string password)
        {
            // !! IMPORTANT COPY OF THIS CODE IS IN ENTITY FOR SEED USER CREATION
            string Salt = "NZsP7NnmfBuYeJrRAKNuVQ==";

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: password,
                    salt: Encoding.ASCII.GetBytes(Salt),
                    prf: KeyDerivationPrf.HMACSHA1,
                    iterationCount: 10000,
                    numBytesRequested: 256 / 8));
            return hashed;
        }

    }
}

