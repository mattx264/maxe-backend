﻿using Microsoft.EntityFrameworkCore;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.Entity.ExamRunModels;
using POL.Maxe.Entity.User;

namespace POL.Maxe.DataAccess.DatabaseContext
{
    public partial class MaxeDbContext : DbContext
    {
        public MaxeDbContext(DbContextOptions<MaxeDbContext> options) : base(options) { }
        public DbSet<UserModel> UserModel { get; set; }
        public DbSet<ExamModel> ExamModel { get; set; }
        public DbSet<ExamRunModel> ExamRunModel { get; set; }
        public DbSet<FileModel> FileModel { get; set; }
    }
}
