
IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [CategoryExam] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [ParentCategoryExamId] int NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_CategoryExam] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_CategoryExam_CategoryExam_ParentCategoryExamId] FOREIGN KEY ([ParentCategoryExamId]) REFERENCES [CategoryExam] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [UserModel] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Email] nvarchar(max) NULL,
    [Password] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [Avator] nvarchar(max) NULL,
    CONSTRAINT [PK_UserModel] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [ExamRunModel] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL,
    [Language] nvarchar(max) NULL,
    [CurrentQuestionIndex] int NOT NULL,
    [UserModelId] int NULL,
    [QuestionRepeat] int NULL,
    [ExamType] int NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_ExamRunModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ExamRunModel_UserModel_UserModelId] FOREIGN KEY ([UserModelId]) REFERENCES [UserModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [FileModel] (
    [Id] int NOT NULL IDENTITY,
    [FilePath] nvarchar(max) NULL,
    [FileName] nvarchar(max) NULL,
    [IsPrivate] bit NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [UserModelId] int NOT NULL,
    [Extension] nvarchar(max) NULL,
    [GuidName] nvarchar(max) NULL,
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_FileModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_FileModel_UserModel_UserModelId] FOREIGN KEY ([UserModelId]) REFERENCES [UserModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ExamModel] (
    [Id] int NOT NULL IDENTITY,
    [Title] nvarchar(max) NULL,
    [CategoryExamId] int NOT NULL,
    [Description] nvarchar(max) NOT NULL,
    [Tags] nvarchar(max) NULL,
    [FileModelId] int NOT NULL,
    [UserModelId] int NOT NULL,
    [Language] nvarchar(max) NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_ExamModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ExamModel_CategoryExam_CategoryExamId] FOREIGN KEY ([CategoryExamId]) REFERENCES [CategoryExam] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_ExamModel_FileModel_FileModelId] FOREIGN KEY ([FileModelId]) REFERENCES [FileModel] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_ExamModel_UserModel_UserModelId] FOREIGN KEY ([UserModelId]) REFERENCES [UserModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [QuestionModel] (
    [Id] int NOT NULL IDENTITY,
    [Note] nvarchar(max) NULL,
    [ExamModelId] int NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_QuestionModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_QuestionModel_ExamModel_ExamModelId] FOREIGN KEY ([ExamModelId]) REFERENCES [ExamModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [AnswerModel] (
    [Id] int NOT NULL IDENTITY,
    [AnswerType] int NOT NULL,
    [DifficultyLevel] int NOT NULL,
    [QuestionModelId] int NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_AnswerModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AnswerModel_QuestionModel_QuestionModelId] FOREIGN KEY ([QuestionModelId]) REFERENCES [QuestionModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ContentSectionCorrectAnswer] (
    [Id] int NOT NULL IDENTITY,
    [QuestionModelId] int NOT NULL,
    [Value] nvarchar(max) NULL,
    [Type] int NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_ContentSectionCorrectAnswer] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ContentSectionCorrectAnswer_QuestionModel_QuestionModelId] FOREIGN KEY ([QuestionModelId]) REFERENCES [QuestionModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ContentSectionQuestion] (
    [Id] int NOT NULL IDENTITY,
    [Value] nvarchar(max) NULL,
    [Type] int NOT NULL,
    [QuestionModelId] int NOT NULL,
    [FileModelId] int NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_ContentSectionQuestion] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ContentSectionQuestion_FileModel_FileModelId] FOREIGN KEY ([FileModelId]) REFERENCES [FileModel] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_ContentSectionQuestion_QuestionModel_QuestionModelId] FOREIGN KEY ([QuestionModelId]) REFERENCES [QuestionModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [RunQuestionModel] (
    [Id] int NOT NULL IDENTITY,
    [QuestionId] int NOT NULL,
    [IsQuestionPlayed] bit NOT NULL,
    [RememberQuestionModel] int NOT NULL,
    [UserAnswer] nvarchar(max) NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [ExamRunModelId] int NULL,
    CONSTRAINT [PK_RunQuestionModel] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RunQuestionModel_ExamRunModel_ExamRunModelId] FOREIGN KEY ([ExamRunModelId]) REFERENCES [ExamRunModel] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_RunQuestionModel_QuestionModel_QuestionId] FOREIGN KEY ([QuestionId]) REFERENCES [QuestionModel] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ContentSectionAnswer] (
    [Id] int NOT NULL IDENTITY,
    [AnswerModelId] int NOT NULL,
    [Value] nvarchar(max) NULL,
    [Type] int NOT NULL,
    [IsActive] bit NOT NULL DEFAULT CAST(1 AS bit),
    [CreatedBy] nvarchar(max) NULL DEFAULT N'system',
    [ModifiedBy] nvarchar(max) NULL DEFAULT N'system',
    [DateAdded] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    [DateModified] datetime2 NOT NULL DEFAULT '2020-01-01T00:00:00.0000000',
    CONSTRAINT [PK_ContentSectionAnswer] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ContentSectionAnswer_AnswerModel_AnswerModelId] FOREIGN KEY ([AnswerModelId]) REFERENCES [AnswerModel] ([Id]) ON DELETE NO ACTION
);
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] ON;
INSERT INTO [CategoryExam] ([Id], [Name], [ParentCategoryExamId])
VALUES (1, N'General', NULL),
(2, N'Politics', NULL),
(3, N'Art', NULL),
(4, N'Music', 4),
(5, N'Philosophy, Religion, & Theology', NULL),
(6, N'Biological', NULL),
(7, N'Military', NULL),
(8, N'Physical Sciences', NULL),
(9, N'Literature', NULL),
(10, N'Language', NULL),
(11, N'Engineering', NULL),
(12, N'Automobile', 12),
(13, N'Law', NULL),
(14, N'Mathematics', NULL),
(15, N'Geographic', NULL),
(16, N'History', NULL),
(17, N'Chemistry', NULL),
(18, N'IT', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Avator', N'Email', N'FirstName', N'Guid', N'LastName', N'Password', N'PhoneNumber') AND [object_id] = OBJECT_ID(N'[UserModel]'))
    SET IDENTITY_INSERT [UserModel] ON;
INSERT INTO [UserModel] ([Id], [Avator], [Email], [FirstName], [Guid], [LastName], [Password], [PhoneNumber])
VALUES (1, N'Default1', N'test@test.com', N'Joe', 'c8366a69-ce09-4591-9399-64e831e991e4', N'Test', N'5lWUcAhyHCV2rTZqqyE8JIMZJjjAwlwRMrq5jxH+KQY=', N'5555555555');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Avator', N'Email', N'FirstName', N'Guid', N'LastName', N'Password', N'PhoneNumber') AND [object_id] = OBJECT_ID(N'[UserModel]'))
    SET IDENTITY_INSERT [UserModel] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] ON;
INSERT INTO [CategoryExam] ([Id], [Name], [ParentCategoryExamId])
VALUES (19, N'Programing', 18),
(35, N'Network', 18);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Extension', N'FileName', N'FilePath', N'GuidName', N'IsPrivate', N'UserModelId') AND [object_id] = OBJECT_ID(N'[FileModel]'))
    SET IDENTITY_INSERT [FileModel] ON;
INSERT INTO [FileModel] ([Id], [Extension], [FileName], [FilePath], [GuidName], [IsPrivate], [UserModelId])
VALUES (1, N'jpg', N'abc.jpg', N'demo/abc.jpg', N'abc', CAST(0 AS bit), 1),
(2, N'jpg', N'code.jpg', N'demo/code.jpg', N'code', CAST(0 AS bit), 1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Extension', N'FileName', N'FilePath', N'GuidName', N'IsPrivate', N'UserModelId') AND [object_id] = OBJECT_ID(N'[FileModel]'))
    SET IDENTITY_INSERT [FileModel] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] ON;
INSERT INTO [CategoryExam] ([Id], [Name], [ParentCategoryExamId])
VALUES (20, N'Assebly', 19),
(21, N'C', 19),
(22, N'C++', 19),
(23, N'C#', 19),
(24, N'CSS', 19),
(25, N'GO', 19),
(26, N'Html', 19),
(27, N'Java', 19),
(28, N'JavaScript', 19),
(29, N'Katlin', 19),
(30, N'PHP', 19),
(31, N'Python', 19),
(32, N'R', 19),
(33, N'Swift', 19),
(34, N'TypeScript', 19);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Name', N'ParentCategoryExamId') AND [object_id] = OBJECT_ID(N'[CategoryExam]'))
    SET IDENTITY_INSERT [CategoryExam] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CategoryExamId', N'Description', N'FileModelId', N'Language', N'Tags', N'Title', N'UserModelId') AND [object_id] = OBJECT_ID(N'[ExamModel]'))
    SET IDENTITY_INSERT [ExamModel] ON;
INSERT INTO [ExamModel] ([Id], [CategoryExamId], [Description], [FileModelId], [Language], [Tags], [Title], [UserModelId])
VALUES (1, 1, N'abcd', 1, N'en', N'', N'abcd', 1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CategoryExamId', N'Description', N'FileModelId', N'Language', N'Tags', N'Title', N'UserModelId') AND [object_id] = OBJECT_ID(N'[ExamModel]'))
    SET IDENTITY_INSERT [ExamModel] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ExamModelId', N'Note') AND [object_id] = OBJECT_ID(N'[QuestionModel]'))
    SET IDENTITY_INSERT [QuestionModel] ON;
INSERT INTO [QuestionModel] ([Id], [ExamModelId], [Note])
VALUES (1, 1, NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ExamModelId', N'Note') AND [object_id] = OBJECT_ID(N'[QuestionModel]'))
    SET IDENTITY_INSERT [QuestionModel] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerType', N'DifficultyLevel', N'QuestionModelId') AND [object_id] = OBJECT_ID(N'[AnswerModel]'))
    SET IDENTITY_INSERT [AnswerModel] ON;
INSERT INTO [AnswerModel] ([Id], [AnswerType], [DifficultyLevel], [QuestionModelId])
VALUES (1, 1, 2, 1),
(2, 7, 4, 1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerType', N'DifficultyLevel', N'QuestionModelId') AND [object_id] = OBJECT_ID(N'[AnswerModel]'))
    SET IDENTITY_INSERT [AnswerModel] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'QuestionModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionCorrectAnswer]'))
    SET IDENTITY_INSERT [ContentSectionCorrectAnswer] ON;
INSERT INTO [ContentSectionCorrectAnswer] ([Id], [QuestionModelId], [Type], [Value])
VALUES (1, 1, 5, N'Earth');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'QuestionModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionCorrectAnswer]'))
    SET IDENTITY_INSERT [ContentSectionCorrectAnswer] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FileModelId', N'QuestionModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionQuestion]'))
    SET IDENTITY_INSERT [ContentSectionQuestion] ON;
INSERT INTO [ContentSectionQuestion] ([Id], [FileModelId], [QuestionModelId], [Type], [Value])
VALUES (1, NULL, 1, 0, N'On what planet we live on ?');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'FileModelId', N'QuestionModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionQuestion]'))
    SET IDENTITY_INSERT [ContentSectionQuestion] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] ON;
INSERT INTO [ContentSectionAnswer] ([Id], [AnswerModelId], [Type], [Value])
VALUES (1, 1, 5, N'Mars');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] ON;
INSERT INTO [ContentSectionAnswer] ([Id], [AnswerModelId], [Type], [Value])
VALUES (2, 1, 5, N'Venus');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] ON;
INSERT INTO [ContentSectionAnswer] ([Id], [AnswerModelId], [Type], [Value])
VALUES (3, 1, 5, N'Sun');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AnswerModelId', N'Type', N'Value') AND [object_id] = OBJECT_ID(N'[ContentSectionAnswer]'))
    SET IDENTITY_INSERT [ContentSectionAnswer] OFF;
GO

CREATE INDEX [IX_AnswerModel_QuestionModelId] ON [AnswerModel] ([QuestionModelId]);
GO

CREATE INDEX [IX_CategoryExam_ParentCategoryExamId] ON [CategoryExam] ([ParentCategoryExamId]);
GO

CREATE INDEX [IX_ContentSectionAnswer_AnswerModelId] ON [ContentSectionAnswer] ([AnswerModelId]);
GO

CREATE INDEX [IX_ContentSectionCorrectAnswer_QuestionModelId] ON [ContentSectionCorrectAnswer] ([QuestionModelId]);
GO

CREATE INDEX [IX_ContentSectionQuestion_FileModelId] ON [ContentSectionQuestion] ([FileModelId]);
GO

CREATE INDEX [IX_ContentSectionQuestion_QuestionModelId] ON [ContentSectionQuestion] ([QuestionModelId]);
GO

CREATE INDEX [IX_ExamModel_CategoryExamId] ON [ExamModel] ([CategoryExamId]);
GO

CREATE INDEX [IX_ExamModel_FileModelId] ON [ExamModel] ([FileModelId]);
GO

CREATE INDEX [IX_ExamModel_UserModelId] ON [ExamModel] ([UserModelId]);
GO

CREATE INDEX [IX_ExamRunModel_UserModelId] ON [ExamRunModel] ([UserModelId]);
GO

CREATE INDEX [IX_FileModel_UserModelId] ON [FileModel] ([UserModelId]);
GO

CREATE INDEX [IX_QuestionModel_ExamModelId] ON [QuestionModel] ([ExamModelId]);
GO

CREATE INDEX [IX_RunQuestionModel_ExamRunModelId] ON [RunQuestionModel] ([ExamRunModelId]);
GO

CREATE INDEX [IX_RunQuestionModel_QuestionId] ON [RunQuestionModel] ([QuestionId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20220417014146_Init', N'6.0.3');
GO

COMMIT;
GO


