﻿using System.Threading.Tasks;

namespace POL.Maxe.DataAccess
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync();
    }

}
