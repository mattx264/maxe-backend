﻿namespace POL.Maxe.WebService.Models.Builder
{
    public class QuizeBuilder
    {
        public string[] Questions { get; set; }
        public string[] CorrectAnswers { get; set; }
        public string[] Answers { get; set; }

    }
}
