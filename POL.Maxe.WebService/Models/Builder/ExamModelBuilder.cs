﻿using POL.Maxe.Entity.ExamModels;
using System.Collections.Generic;

namespace POL.Maxe.WebService.Models.Builder
{
    /// <summary>
    /// Exam Model is 'folder' for question. It's grouping questions by category
    /// </summary>
    public class ExamModelBuilder : ExamModel
    {
        public double? Version { get; set; }
        public string CategoryExamDescription { get; set; }
        public string FileModelGuid { get; set; }
        public List<QuizeBuilder> Quizes { get; set; }
    }
}

