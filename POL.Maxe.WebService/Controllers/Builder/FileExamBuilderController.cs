﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.Common;
using POL.Maxe.WebService.Helpers.Interfaces;
using POL.Maxe.WebService.Models.Builder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers.Builder
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileExamBuilderController : ControllerBase
    {
        private readonly IExamModelRepository _examModelRepository;
        private readonly ICategoryExamRepository _categoryExamRepository;
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUnitOfWork _unitOfWork;

        public FileExamBuilderController(IExamModelRepository examModelRepository,
            ICategoryExamRepository categoryExamRepository,
             IAuthenticationHelper authenticationHelper,
            IUnitOfWork unitOfWork)
        {
            _examModelRepository = examModelRepository;
            _categoryExamRepository = categoryExamRepository;
            _authenticationHelper = authenticationHelper;
            _unitOfWork = unitOfWork;
        }
        [HttpPost, DisableRequestSizeLimit]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UploadExamBulkFileAsync(
         IFormFile file,
         CancellationToken cancellationToken)
        {
            var userModelId = _authenticationHelper.GetUser(User).Id;
            string path = Path.Combine(Environment.CurrentDirectory, "Uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension.ToLower() != ".csv" && fileExtension.ToLower() != ".json")
            {
                return BadRequest("Wrong Extension");
            }

            string fileName = Path.GetFileName(file.FileName);
            string filePath = Path.Combine(path, fileName);
            using (FileStream stream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            string fileData = System.IO.File.ReadAllText(filePath);
            try
            {
                ExamModel examModel = null;
                if (fileExtension.ToLower() == ".csv")
                {
                    examModel = ProcessCsvFile(fileData);
                }
                else if (fileExtension.ToLower() == ".json")
                {
                    examModel = ProcessJsonSchemaFile(fileData);
                }
                examModel.UserModelId = userModelId;
                _examModelRepository.Create(examModel);

                await _unitOfWork.SaveChangesAsync();
            }
            catch (ImportExamException exception)
            {
                return BadRequest(exception.Message);
            }
            return Ok();
        }
        [Obsolete(message: "This method is obsolete on favor of ProcessJsonSchemaFile")]
        private ExamModel ProcessJsonFile(string fileData)
        {
            var examModel = JsonConvert.DeserializeObject<ExamModel>(fileData);

            return examModel;
        }

        private ExamModel ProcessJsonSchemaFile(string fileData)
        {
            var tempExamModel = JsonConvert.DeserializeObject<ExamModelBuilder>(fileData);
            if (tempExamModel.Version == null)
            {
                return ProcessJsonFile(fileData);
            }
            else if (tempExamModel.Version == 1)
            {
                return ProcessVersion_1(tempExamModel);
            }
            var examModel = JsonConvert.DeserializeObject<ExamModel>(fileData);

            return examModel;
        }

        private ExamModel ProcessVersion_1(ExamModelBuilder tempExamModel)
        {
            if (tempExamModel is null)
            {
                throw new ArgumentNullException(nameof(tempExamModel));
            }
            var categoryExam = _categoryExamRepository.FindByCondition(x => x.Name == tempExamModel.CategoryExamDescription).FirstOrDefault();
            if (categoryExam == null)
            {
                throw new ImportExamException($"Category exam not found for name: '{tempExamModel.CategoryExam}' ");
            }
            tempExamModel.CategoryExamId = categoryExam.Id;
            QuizProcess(tempExamModel);

            if (tempExamModel.FileModelGuid != null)
            {
                throw new ImportExamException($"FileModelGuid is not implemented yet !!!");
            }


            return tempExamModel;
        }

        private static void QuizProcess(ExamModelBuilder tempExamModel)
        {
            if (tempExamModel.Quizes != null && tempExamModel.Quizes.Count > 0)
            {
                foreach (var quiz in tempExamModel.Quizes)
                {
                    var questionModel = new QuestionModel()
                    {
                        QuestionSections = new List<ContentSectionQuestion>(),
                        ContentSectionCorrectAnswers = new List<ContentSectionCorrectAnswer>(),
                        AnswerList = new List<AnswerModel>()
                    };
                    foreach (var item in quiz.Questions)
                    {
                        questionModel.QuestionSections.Add(new ContentSectionQuestion()
                        {
                            Value = item,
                            Type = ContentSectionQuestionType.TEXT
                        });
                    }

                    questionModel.AnswerList.Add(new AnswerModel()
                    {
                        WrongAnswers = quiz.Answers
                            .Select(x => new ContentSectionAnswer() { Value = x, Type = ContentSectionAnswerType.TEXT })
                            .ToList(),
                        AnswerType = AnswerType.QUIZ,
                        DifficultyLevel = DifficultyLevel.Normal
                    });

                    foreach (var item in quiz.CorrectAnswers)
                    {
                        questionModel.ContentSectionCorrectAnswers.Add(new ContentSectionCorrectAnswer()
                        {
                            Value = item,
                            Type = ContentSectionAnswerType.TEXT
                        });
                    }

                    tempExamModel.QuestionModels.Add(questionModel);
                }
            }
        }

        public ExamModel ProcessCsvFile(string csvData)
        {
            csvData = csvData.Replace("\"\"", "\"");
            csvData = csvData.Replace("\"[", "[");
            csvData = csvData.Replace("]\"", "]");

            //TODO move this to sepperate file, keep in mind that Unit tests will have to be move as well.
            const char separator = '|';
            var examModel = new ExamModel();
            examModel.QuestionModels = new List<QuestionModel>();
            QuestionModel question = null;
            List<ContentSectionAnswer> wrongAnswer = null;
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        var columns = row.Split(separator);
                        if (columns[0] == "info")
                        {
                            examModel.Title = columns[1];
                            var categoryExam = _categoryExamRepository.FindByCondition(x => x.Name == columns[2].Trim()).FirstOrDefault();
                            if (categoryExam == null)
                            {
                                throw new ImportExamException($"Category exam not found for name: '{columns[2]}' ");
                            }

                            examModel.CategoryExamId = categoryExam.Id;
                            if (columns.Length == 3)
                            {
                                throw new ImportExamException("Exam description is missing");
                            }
                            examModel.Description = columns[3];
                            if (columns.Length == 4 || string.IsNullOrEmpty(columns[4]))
                            {
                                examModel.FileModelId = 1;//add defaulr image
                            }
                            else
                            {
                                //todo this has to be change insted of id it should be hash plus check if file can be use by user
                                int fileModelId = int.Parse(columns[4]);
                                examModel.FileModelId = fileModelId;
                            }


                            if (columns.Length > 3)
                            {
                                examModel.Language = "en";
                            }
                            else
                            {
                                examModel.Language = columns[5].ToLower();

                            }
                        }
                        else if (columns[0] == "question")
                        {
                            if (question == null || question.ContentSectionCorrectAnswers != null)
                            {
                                //it should have at least one corect answer
                                question = new QuestionModel();
                                question.QuestionSections = new List<ContentSectionQuestion>();
                                examModel.QuestionModels.Add(question);

                            }
                            var type = (ContentSectionQuestionType)Enum.Parse(typeof(ContentSectionQuestionType), columns[1]);
                            if (type == ContentSectionQuestionType.SVG || type == ContentSectionQuestionType.IMAGE)
                            {
                                try
                                {
                                    var fileModelId = int.Parse(columns[3]);
                                    question.QuestionSections.Add(new ContentSectionQuestion()
                                    {
                                        Value = null,
                                        FileModelId = fileModelId,
                                        Type = (ContentSectionQuestionType)Enum.Parse(typeof(ContentSectionQuestionType), columns[1]),
                                    });
                                }
                                catch (IndexOutOfRangeException ex)
                                {
                                    throw new ImportExamException($"File Id is missing");
                                }
                            }
                            else
                            {
                                question.QuestionSections.Add(new ContentSectionQuestion()
                                {
                                    Value = columns[2],
                                    Type = (ContentSectionQuestionType)Enum.Parse(typeof(ContentSectionQuestionType), columns[1]),
                                });
                            }

                            question.AnswerList = new List<AnswerModel>();

                        }
                        else if (columns[0] == "correctAnswer")
                        {
                            if (question.ContentSectionCorrectAnswers == null)
                            {
                                question.ContentSectionCorrectAnswers = new List<ContentSectionCorrectAnswer>();
                            }
                            var contentSectionAnswerType = (ContentSectionAnswerType)Enum.Parse(typeof(ContentSectionAnswerType), columns[1]);

                            question.ContentSectionCorrectAnswers.Add(new ContentSectionCorrectAnswer()
                            {
                                Value = columns[2],
                                Type = contentSectionAnswerType
                            });
                            //question.CorrectAnswerId
                        }
                        else if (columns[0] == "wrongAnswer")
                        {
                            if (wrongAnswer == null)
                            {
                                wrongAnswer = new List<ContentSectionAnswer>();
                            }
                            wrongAnswer.Add(new ContentSectionAnswer()
                            {
                                Value = columns[2],
                                Type = (ContentSectionAnswerType)Enum.Parse(typeof(ContentSectionAnswerType), columns[1])
                            });
                        }
                        else if (columns[0] == "answer")
                        {
                            var answerModel = new AnswerModel()
                            {
                                WrongAnswers = wrongAnswer,
                                AnswerType = (AnswerType)Enum.Parse(typeof(AnswerType), columns[1]),
                                DifficultyLevel = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel), columns[2]),

                            };

                            question.AnswerList.Add(answerModel);
                            wrongAnswer = null;
                        }
                    }

                }

            }
            return examModel;
        }
    }
}
