﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.Helpers.Interfaces;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers.Builder
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class BuilderSearchController : ControllerBase
    {
        private readonly IExamModelRepository _examModelRepository;
        private readonly IAuthenticationHelper _authenticationHelper;

        public BuilderSearchController(IExamModelRepository examModelRepository, IAuthenticationHelper authenticationHelper)
        {
            _examModelRepository = examModelRepository;
            _authenticationHelper = authenticationHelper;

        }
        [HttpGet]
        public IList<ExamSearchViewModel> GetByUser()
        {
            var userModelId = _authenticationHelper.GetUser(User).Id;
            var examModel = _examModelRepository.GetByUser(userModelId);

            return examModel.Select(x => new ExamSearchViewModel(x, HttpContext.Request)).ToList();
        }
    }
}
