﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.Helpers.Interfaces;
using POL.Maxe.WebService.ViewModels.Builder;
using POL.Maxe.WebService.ViewModels.Exams;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers.Builder
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ExamsBuilderController : ControllerBase
    {
        private readonly IExamModelRepository _examModelRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUnitOfWork _unitOfWork;

        public ExamsBuilderController(IExamModelRepository examModelRepository, IQuestionRepository questionRepository, IAuthenticationHelper authenticationHelper, IUnitOfWork unitOfWork)
        {
            _examModelRepository = examModelRepository;
            _questionRepository = questionRepository;
            _authenticationHelper = authenticationHelper;
            _unitOfWork = unitOfWork;

        }
        [HttpGet]
        public BuilderExamViewModel Get(int examId)
        {
            if (examId == 0)
            {
                throw new ArgumentException("Incorent exam Id: " + examId);
            }
            var userModelId = _authenticationHelper.GetUser(User).Id;
            var examModel = _examModelRepository.FindByIdAllActive(examId);
            if (examModel == null)
            {
                Log.Error("Exam not found for id:" + examId.ToString());
                return null;
            }
            if (examModel.UserModelId != userModelId)
            {
                Log.Error($"User is not owner of exam, exmaid: + {examId}, exam id: {examModel.Id}");

            }
            return new BuilderExamViewModel(examModel, HttpContext.Request);
        }
        [HttpPost]
        public async Task<int> PostAsync(BuilderExamViewModel builderExamViewModel)
        {
            var userModelId = _authenticationHelper.GetUser(User).Id;

            var builderExam = builderExamViewModel.ToExamModel();
            builderExam.UserModelId = userModelId;
            _examModelRepository.Create(builderExam);
            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
            return builderExam.Id;
        }
        [HttpPut]
        public async Task PutAsync(BuilderExamViewModel builderExamViewModel)
        {
            var builderExam = _examModelRepository.FindById(builderExamViewModel.Id);
            builderExam.Title = builderExamViewModel.Title;
            builderExam.Description = builderExamViewModel.Description;
            builderExam.CategoryExamId = builderExamViewModel.CategoryExamId;
            builderExam.FileModelId = builderExamViewModel.FileModelId;

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        [HttpPut("Question")]
        public async Task<int> PutQuestionAsync(QuestionModelBackUp questionModelBackUp)
        {

            //(builderExamViewModel.ToExamModel());
            try
            {
                if (questionModelBackUp.Id == 0)
                {
                    var newQuestion = questionModelBackUp.ToQuestionModel();
                    _questionRepository.Create(newQuestion);
                    await _unitOfWork.SaveChangesAsync();
                    return newQuestion.Id;
                }
                var questionModel = _questionRepository.FindById(questionModelBackUp.Id);

                #region ContentSectionCorrectAnswers

                for (int i = 0; i < questionModel.ContentSectionCorrectAnswers.Count; i++)
                {
                    var questionSection = questionModel.ContentSectionCorrectAnswers[i];
                    var qm = questionModelBackUp.ContentSectionCorrectAnswers.FirstOrDefault(x => x.Id == questionSection.Id);
                    if (qm == null)
                    {
                        questionSection.IsActive = false;
                    }
                    else
                    {
                        questionSection.Value = qm.Value;
                        questionSection.Type = qm.Type;
                    }
                }

                foreach (var questionSection in questionModelBackUp.ContentSectionCorrectAnswers.Where(x => x.Id == 0))
                {
                    questionModel.ContentSectionCorrectAnswers.Add(new ContentSectionCorrectAnswer()
                    {
                        Type = questionSection.Type,
                        Value = questionSection.Value
                    });
                }
                #endregion
                #region QuestionSections
                for (int i = 0; i < questionModel.QuestionSections.Count; i++)
                {
                    var questionSection = questionModel.QuestionSections[i];
                    var qm = questionModelBackUp.QuestionSections.FirstOrDefault(x => x.Id == questionSection.Id);
                    if (qm == null)
                    {
                        questionSection.IsActive = false;
                    }
                    else
                    {
                        questionSection.Value = qm.Value;
                        questionSection.Type = qm.Type;
                    }
                }

                foreach (var questionSection in questionModelBackUp.QuestionSections.Where(x => x.Id == 0))
                {
                    questionModel.QuestionSections.Add(new ContentSectionQuestion()
                    {
                        Type = questionSection.Type,
                        Value = questionSection.Value
                    });
                }
                #endregion
                #region AnswerList
                for (int i = 0; i < questionModel.AnswerList.Count; i++)
                {
                    var answerList = questionModel.AnswerList[i];
                    var qm = questionModelBackUp.AnswerList.FirstOrDefault(x => x.Id == answerList.Id);
                    if (qm == null)
                    {
                        answerList.IsActive = false;
                        if (answerList.WrongAnswers != null)
                        {
                            foreach (var item in answerList.WrongAnswers)
                            {
                                item.IsActive = false;
                            }
                        }
                    }
                    else
                    {
                        answerList.AnswerType = qm.AnswerType;
                        if (qm.WrongAnswers != null)
                        {

                            for (int j = 0; j < answerList.WrongAnswers.Count; j++)
                            {
                                var wrongAnswer = answerList.WrongAnswers[j];
                                var wa = qm.WrongAnswers.FirstOrDefault(x => x.Id == wrongAnswer.Id);

                                if (wa == null)
                                {
                                    wrongAnswer.IsActive = false;
                                }
                                else
                                {
                                    wrongAnswer.Type = wa.Type;
                                    wrongAnswer.Value = wa.Value;
                                }
                            }
                            foreach (var wrongAnswer in qm.WrongAnswers.Where(x => x.Id == 0))
                            {
                                answerList.WrongAnswers.Add(new ContentSectionAnswer()
                                {
                                    Type = wrongAnswer.Type,
                                    Value = wrongAnswer.Value
                                });
                            }
                        }
                    }
                }

                foreach (var answerList in questionModelBackUp.AnswerList.Where(x => x.Id == 0))
                {

                    questionModel.AnswerList.Add(new AnswerModelBackup()
                    {
                        AnswerType = answerList.AnswerType,
                        WrongAnswers = answerList.WrongAnswers
                    });
                }
                #endregion
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
            return questionModelBackUp.Id;
        }
        [HttpDelete]
        public async Task<OkResult> DeleteAsync(int id)
        {
            var questionModel = _questionRepository.FindById(id);

            _questionRepository.Delete(questionModel);
            await _unitOfWork.SaveChangesAsync();
            return Ok();
        }
    }
}
