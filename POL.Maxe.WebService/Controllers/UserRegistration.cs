using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.User;
using POL.Maxe.WebService.Helpers;
using POL.Maxe.WebService.ViewModels.User;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class UserRegistration : Controller
    {
        private readonly IUserModelRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserRegistration(IUserModelRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        public async Task<IActionResult> RegistrationAsync([FromBody] RegistrationViewModel viewModel)
        {
            if (_userRepository.FindByCondition(x => x.Email == viewModel.Email).Any())
            {
                return Ok(new
                {
                    message = "Email exists"
                });
            }


            if (String.IsNullOrWhiteSpace(viewModel.Password))
            {
                return Ok(new
                {
                    message = "Password cannot be empty"
                });
            }
            _userRepository.Create(new UserModel
            {
                Email = viewModel.Email,
                Password = UserHelper.PasswordHash(viewModel.Password),
                Guid = Guid.NewGuid(),
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName,
                //TimeZone = viewModel.TimeZone
            });
            await _unitOfWork.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("CheckEmail/{email}")]
        public IActionResult CheckEmail(string email)
        {
            if (_userRepository.FindByCondition(x => x.Email == email).Any())
            {
                return Ok(new
                {
                    message = "Email exists"
                });
            }
            return Ok(new { message = "Email does not exists" });
        }
    }
}