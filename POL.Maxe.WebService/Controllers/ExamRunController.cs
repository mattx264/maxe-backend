﻿using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamRunModels;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamRunController : ControllerBase
    {
        private IExamRunModelRepository _examRunModelRepository;
        private IExamModelRepository _examModelRepository;

        private IUnitOfWork _unitOfWork;


        public ExamRunController(IExamRunModelRepository examRunModelRepository,
            IExamModelRepository examModelRepository,
            IUnitOfWork unitOfWork)
        {
            _examRunModelRepository = examRunModelRepository;
            _examModelRepository = examModelRepository;
            _unitOfWork = unitOfWork;
        }
        //get current question
        //get count of questions

        //submit answer

        //start new exam
        [HttpPut("start")]
        public async Task<Guid> StartAsync(SaveExamRunViewModel saveExamRunViewModel)
        {
            var exams = _examModelRepository.FindByCondition(x => saveExamRunViewModel.ExamIds.Contains(x.Id));
            var questionIds = exams
                .SelectMany(x => x.QuestionModels.Select(x => x.Id))
                .ToList();


            var guid = Guid.NewGuid();
            _examRunModelRepository.Create(new ExamRunModel()
            {
                Guid = guid,
                CurrentQuestionIndex = 0,
                QuestionRepeat = saveExamRunViewModel.QuestionRepeat,
                ExamType = saveExamRunViewModel.ExamType,
                //TODO right now is only first language what it makes for mostyl tests.
                Language = exams.First().Language,
                RunQuestionModels = questionIds.Select(id => new RunQuestionModel()
                {
                    QuestionId = id,
                    IsQuestionPlayed = false
                }).ToList()
            });
            await _unitOfWork.SaveChangesAsync();

            return guid;
        }
        [HttpGet("get")]
        public ExamRunViewModel Get(Guid guid)
        {
            bool isGuidValid = Guid.Empty == guid;
            if (isGuidValid)
            {
                throw new ArgumentNullException(nameof(guid));
            }

            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();

            //todo check if user that make request has access to examRun
            var examRunViewModel = new ExamRunViewModel(examRun);

            return examRunViewModel;

        }
    }
}
