﻿using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.Enums;
using POL.Maxe.WebService.ViewModels;
using POL.Maxe.WebService.ViewModels.Question;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IExamRunModelRepository _examModelRepository;
        private readonly IExamRunModelRepository _examRunModelRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public QuestionController(IExamRunModelRepository examModelRepository,
            IExamRunModelRepository examRunModelRepository,
            IQuestionRepository questionRepository,
            IUnitOfWork unitOfWork
            )
        {
            _examModelRepository = examModelRepository;
            _examRunModelRepository = examRunModelRepository;
            _questionRepository = questionRepository;
            _unitOfWork = unitOfWork;
        }

        [HttpGet("GetQuestion")]
        public ActionResult<QuestionSessionViewModel> GetQuestion(Guid guid)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(guid));
            }

            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();
            if (examRun == null)
            {
                return NotFound("Question not found");
            }
            if (examRun.RunQuestionModels[examRun.CurrentQuestionIndex].UserAnswer != null)
            {
                if (examRun.RunQuestionModels.Count == examRun.CurrentQuestionIndex + 1)
                {
                    return Ok();
                }
            }
            var question = _questionRepository.FindById(examRun.RunQuestionModels[examRun.CurrentQuestionIndex].QuestionId);

            var answer = question.AnswerList.First(x => x.DifficultyLevel == DifficultyLevel.Normal);

            var questionViewModel = new QuestionSessionViewModel(question, answer,HttpContext.Request);
            return Ok(questionViewModel);

        }
        [HttpGet("GetNextQuestion")]
        public async Task<ActionResult<QuestionSessionViewModel>> GetNextQuestionAsync(Guid guid)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(guid));
            }

            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();
            if (examRun == null)
            {
                return NotFound();
            }

            if (examRun.RunQuestionModels[examRun.CurrentQuestionIndex].UserAnswer != null || examRun.ExamType == ExamTypeEnum.Learn)
            {
                if (examRun.RunQuestionModels.Count == examRun.CurrentQuestionIndex + 1)
                {
                    return Ok();
                }
                examRun.CurrentQuestionIndex++;


            }
            var question = _questionRepository.FindById(examRun.RunQuestionModels[examRun.CurrentQuestionIndex].QuestionId);

            var answer = question.AnswerList.First(x => x.DifficultyLevel == DifficultyLevel.Normal);

            var questionViewModel = new QuestionSessionViewModel(question, answer,HttpContext.Request);
            await _unitOfWork.SaveChangesAsync();
            return Ok(questionViewModel);
        }
        [HttpGet("GetCurrentQuestionAnswer")]
        public ActionResult<IList<AnswerViewModel>> GetCurrentQuestionAnswer(Guid guid)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(guid));
            }
            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();
            var question = _questionRepository.FindById(examRun.RunQuestionModels[examRun.CurrentQuestionIndex].QuestionId);
            return question.ContentSectionCorrectAnswers.Select(x=> new AnswerViewModel(x)).ToList();
        }

        [HttpGet("GetResult")]
        public ActionResult<IList<ExamResultViewModel>> GetResult(Guid guid)
        {
            if (guid == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(guid));
            }
            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();
            var examResultViewModel = new List<ExamResultViewModel>();
            foreach (var runQuestionModel in examRun.RunQuestionModels)
            {

                examResultViewModel.Add(new ExamResultViewModel(runQuestionModel,HttpContext.Request));
            }

            return Ok(examResultViewModel);
        }

        [HttpPost("Answer")]
        public async Task<ActionResult> AnswerAsync(Guid guid, string answer)
        {
            if (answer is null)
            {
                throw new ArgumentNullException(nameof(answer));
            }
            var examRun = _examRunModelRepository.FindByCondition(x => x.Guid == guid).FirstOrDefault();
            examRun.RunQuestionModels[examRun.CurrentQuestionIndex].UserAnswer = answer;
            await _unitOfWork.SaveChangesAsync();
            return Ok();
        }


    }
}
