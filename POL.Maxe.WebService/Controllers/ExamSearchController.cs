﻿using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamSearchController : ControllerBase
    {
        private readonly IExamModelRepository _examModelRepository;

        public ExamSearchController(IExamModelRepository examModelRepository)
        {
            _examModelRepository = examModelRepository;
        }
        [HttpGet("getAll")]
        public IList<ExamSearchViewModel> GetAll()
        {
            var examModel = _examModelRepository.FindAll();
            return examModel.Select(x => new ExamSearchViewModel(x, HttpContext.Request)).ToList();
        }
        [HttpGet("getById")]
        public ExamSearchViewModel GetById(int id)
        {
            var examModel = _examModelRepository.FindById(id);
            return new ExamSearchViewModel(examModel,HttpContext.Request);
        }
    }
}
