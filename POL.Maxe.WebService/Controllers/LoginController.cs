using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.User;
using POL.Maxe.WebService.Helpers;
using POL.Maxe.WebService.ViewModels.User;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class LoginController : Controller
    {
        private IConfiguration _config;
        private IUserModelRepository _userRepository;

        public LoginController(IConfiguration config, IUserModelRepository userRepository)
        {
            _config = config;
            _userRepository = userRepository;
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult<UserViewModel> CreateToken([FromBody] LoginViewModel login)
        {
            if (String.IsNullOrWhiteSpace(login.Password))
            {
                return new UnauthorizedResult();
            }
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                return new UserViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Token = tokenString
                };
            }
            return new UnauthorizedResult();
        }

        private string BuildToken(UserModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claimsForAccessToken = new List<Claim>();
            claimsForAccessToken.Add(new Claim("Guid", user.Guid.ToString()));

            var token = new JwtSecurityToken(issuer: _config["Jwt:Issuer"],
               audience: _config["Jwt:Issuer"],
               claims: claimsForAccessToken.ToArray(),
              expires: DateTime.Now.AddMonths(1),
              signingCredentials: creds

             );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserModel Authenticate(LoginViewModel login)
        {
            UserModel user = _userRepository.Login(login.Email, UserHelper.PasswordHash(login.Password));

            return user;
        }

    }
}