﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.ViewModels.Exams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BackupController : ControllerBase
    {
        private readonly IExamModelRepository _examModelRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BackupController(IExamModelRepository examModelRepository, IUnitOfWork unitOfWork
)
        {
            _examModelRepository = examModelRepository;
            _unitOfWork = unitOfWork;

        }
        [HttpGet()]
        public BackupDataViewModel Get()
        {
            var backup = new BackupDataViewModel();
            var examModel = _examModelRepository.FindAll();
            backup.ExamBackupViewModels = examModel.Select(x => new ExamBackupViewModel(x, HttpContext.Request)).ToList();

            return backup;
        }
        [HttpPost]
        public async Task<int> PostAsync(BackupDataViewModel backupDataViewModel)
        {
            foreach (var examModel in backupDataViewModel.ExamBackupViewModels)
            {

                _examModelRepository.Create(examModel.ToExamModel());
                try
                {
                    await _unitOfWork.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return backupDataViewModel.ExamBackupViewModels.Count;
        }
    }
}
