﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.ViewModels.Exams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryExamController : ControllerBase
    {
        private readonly ICategoryExamRepository _categoryExamRepository;

        public CategoryExamController(ICategoryExamRepository categoryExamRepository)
        {
            _categoryExamRepository = categoryExamRepository;
        }
        [HttpGet]
        public List<CategoryExamViewModel> Get()
        {
            var categories =  _categoryExamRepository.FindAll().Select(x => new CategoryExamViewModel(x)).ToList();

            return categories;
        }
    }
}
