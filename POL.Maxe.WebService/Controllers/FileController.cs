﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.Helpers.Interfaces;
using POL.Maxe.WebService.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private readonly IFileHelper _fileHelper;
        private readonly IFileModelRepository _fileModelRepository;
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUnitOfWork _unitOfWork;

        public FileController(IFileHelper fileHelper, IFileModelRepository fileModelRepository, IAuthenticationHelper authenticationHelper, IUnitOfWork unitOfWork)
        {
            _fileHelper = fileHelper;
            _fileModelRepository = fileModelRepository;
            _authenticationHelper = authenticationHelper;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        public async Task<ActionResult<FileViewModel>> UploadAsync(IFormFile file,
         CancellationToken cancellationToken)
        {
            var fileModel = await _fileHelper.SaveFileAsync(file);
            fileModel.UserModelId = _authenticationHelper.GetUser(User).Id;
            _fileModelRepository.Create(fileModel);
            await _unitOfWork.SaveChangesAsync();
            return new FileViewModel(fileModel, HttpContext.Request);
        }
        [HttpGet]
        public ActionResult<IList<FileViewModel>> GetFiles()
        {
            var userId = _authenticationHelper.GetUser(User).Id;
            var files = _fileModelRepository.FindByCondition(x => x.UserModelId == userId);
            return files.Select(x => new FileViewModel(x, HttpContext.Request)).ToList();
        }
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(int fileId)
        {
            var userId = _authenticationHelper.GetUser(User).Id;
            var file = _fileModelRepository.FindById(fileId);
            if (file.UserModelId != userId)
            {
                return Unauthorized();
            }
            file.IsActive = false;
            await _unitOfWork.SaveChangesAsync();

            return Ok();
                }
    }
}
