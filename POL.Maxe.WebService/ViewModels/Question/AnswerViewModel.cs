﻿using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels.Interfaces;

namespace POL.Maxe.WebService.ViewModels.Question
{
    public class AnswerViewModel
    {
        public AnswerViewModel(IContentSection contentSectionAnswer)
        {

            Type = contentSectionAnswer.Type;
            Value = contentSectionAnswer.Value;
           // SubType = contentSectionAnswer.SubType;
        }

        public ContentSectionAnswerType Type { get; }
        public string Value { get; set; }
       // public ContentSectionAnswerSubType SubType { get; }
    }
}
