﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.ViewModels.Question;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.ViewModels
{
    public class QuestionSessionViewModel
    {

        public QuestionSessionViewModel(QuestionModel question, AnswerModel answer, HttpRequest request)
        {
            if (question is null)
            {
                throw new ArgumentNullException(nameof(question));
            }

            if (answer is null)
            {
                throw new ArgumentNullException(nameof(answer));
            }

            QuestionId = question.Id;
            //if (answer.AnswerType == AnswerType.LIST_DND || answer.AnswerType == AnswerType.LIST_TEXT)
            //{
            //    //list show have tow list one with correct answers and one with wrong
            //    Answers = answer.WrongAnswers.Select(x => new AnswerViewModel(x)).ToList();
            //    var answersJson = JsonSerializer.Deserialize<List<string>>(Answers.First().Value);
            //    answersJson.AddRange(JsonSerializer.Deserialize<List<string>>(question.ContentSectionCorrectAnswer.Value));
            //    Answers.First().Value = JsonSerializer.Serialize(answersJson);
            //}
            //else
            //{
            Answers = answer.WrongAnswers.Select(x => new AnswerViewModel(x)).ToList();
            Answers.AddRange(question.ContentSectionCorrectAnswers.Select(x => new AnswerViewModel(x)).ToList());
            //}

            QuestionSections = question.QuestionSections.Select(x => new QuestionSectionViewModel(x, request)).ToList();
            QuestionType = answer.AnswerType;

        }
        public int QuestionId { get; set; }
        public List<AnswerViewModel> Answers { get; set; }
        public IList<QuestionSectionViewModel> QuestionSections { get; set; }
        public AnswerType QuestionType { get; set; }
        //public string QuestionSubType { get; set; }

        public bool? IsQuestionPlayed { get; set; } = false;
        //public int? AnswerIndex { get; set; } = null;
        public bool? IsMarkAsRemembered { get; set; }
    }
}
