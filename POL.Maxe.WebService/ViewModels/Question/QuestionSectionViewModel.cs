﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.Enums;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.ViewModels.Helpers;
using System;

namespace POL.Maxe.WebService.ViewModels.Question
{
    public class QuestionSectionViewModel
    {
        public QuestionSectionViewModel(ContentSectionQuestion contentSectionQuestion, HttpRequest request)
        {
            if (contentSectionQuestion is null)
            {
                throw new ArgumentNullException(nameof(contentSectionQuestion));
            }

            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            Type = contentSectionQuestion.Type;
            Value = contentSectionQuestion.Value;
            if (contentSectionQuestion.FileModel != null)
            {
                FilePath = $"{FileExtension.GetBaseUrl(request)}/Uploads/{contentSectionQuestion.FileModel.FilePath}";

            }
            //SubType = contentSectionQuestion.SubType;
        }

        public ContentSectionQuestionType Type { get; }
        public string Value { get; }
        public string FilePath { get; private set; }
    }
}
