﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.Entity.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace POL.Maxe.WebService.ViewModels.Question
{
    public class ExamResultViewModel
    {
        public ExamResultViewModel(RunQuestionModel runQuestionModel, HttpRequest request)
        {
            var question = runQuestionModel.Question;
            QuestionSections = question.QuestionSections.Select(x => new QuestionSectionViewModel(x, request)).ToList();
            CorrectAnswers = question.ContentSectionCorrectAnswers.Select(x=>x.Value).ToList();
            UserAnswer = runQuestionModel.UserAnswer;
        }

        public string UserAnswer { get; set; }
        public IList<string> CorrectAnswers { get; set; }
        public List<QuestionSectionViewModel> QuestionSections { get; internal set; }
    }
}
