﻿using Microsoft.AspNetCore.Http;

namespace POL.Maxe.WebService.ViewModels.Helpers
{
    public static class FileExtension
    {
        public static string GetBaseUrl(HttpRequest request)
        {
            var host = request.Host.ToUriComponent();
#if DEBUG
            host = "localhost";
#endif
            var pathBase = request.PathBase.ToUriComponent();

            return $"{request.Scheme}://{host}{pathBase}";
        }
    }
}
