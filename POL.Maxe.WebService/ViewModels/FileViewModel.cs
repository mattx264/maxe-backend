﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.User;
using POL.Maxe.WebService.ViewModels.Helpers;

namespace POL.Maxe.WebService.ViewModels
{
    public class FileViewModel
    {
        public FileViewModel(FileModel fileModel, HttpRequest request)
        {
            Id = fileModel.Id;
            FileName = fileModel.FileName;
            FilePath = $"{FileExtension.GetBaseUrl(request)}/Uploads/{fileModel.FilePath}";
            GuidName = fileModel.GuidName;
            IsPrivate = fileModel.IsPrivate;

        }
       
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string GuidName { get; set; }
        public bool IsPrivate { get; set; }
    }
}
