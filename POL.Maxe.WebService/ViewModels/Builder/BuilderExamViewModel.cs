﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.ViewModels.Exams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.ViewModels.Builder
{
    public class BuilderExamViewModel: ExamBackupViewModel
    {
        public BuilderExamViewModel() { }
        public BuilderExamViewModel(ExamModel examModel, HttpRequest request):base(examModel,request)
        {
           
        }
    }
}
