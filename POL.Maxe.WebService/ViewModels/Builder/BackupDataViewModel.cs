﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.ViewModels.Exams
{
    public class BackupDataViewModel
    {
        public IList<ExamBackupViewModel> ExamBackupViewModels { get; set; }
    }
}
