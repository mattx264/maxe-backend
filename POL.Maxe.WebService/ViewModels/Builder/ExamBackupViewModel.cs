﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.Entity.Interfaces;
using POL.Maxe.WebService.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace POL.Maxe.WebService.ViewModels.Exams
{
    public class ExamBackupViewModel : ExamModel
    {
        public ExamBackupViewModel() { }
        public ExamBackupViewModel(ExamModel examModel, HttpRequest request)
        {
            Title = examModel.Title;
            if (examModel.FileModel != null)
            {
                ImagePath = $"{FileExtension.GetBaseUrl(request)}/Uploads/{examModel.FileModel.FilePath}";
            }
            Id = examModel.Id;
            CategoryExamId = examModel.CategoryExamId;
            Description = examModel.Description;
            QuestionModels = examModel.QuestionModels.Select(x => new QuestionModelBackUp(x)).ToList();
            Tags = examModel.Tags;
            Language = examModel.Language;
            FileModelId = examModel.FileModelId;
            IsActive = examModel.IsActive;
            CreatedBy = examModel.CreatedBy;
            ModifiedBy = examModel.ModifiedBy;
            DateAdded = examModel.DateAdded;
            DateModified = examModel.DateModified;
        }
        internal ExamModel ToExamModel()
        {
            return new ExamModel()
            {
                Id = this.Id,
                Title = Title,
                CategoryExamId = CategoryExamId,
                Description = Description,
                QuestionModels = QuestionModels as List<QuestionModel>,
                Tags = Tags,
                FileModelId = FileModelId,
                UserModelId = UserModelId,
                Language = Language,
                IsActive = IsActive,
                CreatedBy = CreatedBy,
                ModifiedBy = ModifiedBy,
                DateAdded = DateAdded,
                DateModified = DateModified,
            };
        }

       
        public string ImagePath { get; }

#pragma warning disable CS0114 // Member hides inherited member; missing override keyword
        public virtual IList<QuestionModelBackUp> QuestionModels { get; set; }
#pragma warning restore CS0114 // Member hides inherited member; missing override keyword

    }
    public class QuestionModelBackUp : QuestionModel
    {
        public QuestionModelBackUp() { }
        public QuestionModelBackUp(QuestionModel questionModel)
        {
            Id = questionModel.Id;
            QuestionSections = questionModel.QuestionSections.Select(x => new ContentSectionQuestionBackup(x)).ToList();
            AnswerList = questionModel.AnswerList.Select(x => new AnswerModelBackup(x)).ToList();
            ContentSectionCorrectAnswers = questionModel.ContentSectionCorrectAnswers.Select(x => new ContentSectionCorrectAnswerBackup(x)).ToList();
            Note = questionModel.Note;
            ExamModelId = questionModel.ExamModelId;
            IsActive = questionModel.IsActive;
            CreatedBy = questionModel.CreatedBy;
            ModifiedBy = questionModel.ModifiedBy;
            DateAdded = questionModel.DateAdded;
            DateModified = questionModel.DateModified;

        }

        internal QuestionModel ToQuestionModel()
        {
            return new QuestionModel
            {
                Id = Id,
                QuestionSections = QuestionSections.Select(x => x as ContentSectionQuestion).ToList(),
                AnswerList = AnswerList.Select(x => x.ToAnswerList()).ToList(),
                ContentSectionCorrectAnswers = ContentSectionCorrectAnswers.Select(x => x as ContentSectionCorrectAnswer).ToList(),
                Note = Note,
                ExamModelId = ExamModelId,
                IsActive = IsActive,
                CreatedBy = CreatedBy,
                ModifiedBy = ModifiedBy,
                DateAdded = DateAdded,
                DateModified = DateModified
            };
        }

        public virtual IList<ContentSectionCorrectAnswerBackup> ContentSectionCorrectAnswers { get; set; }
        public virtual IList<AnswerModelBackup> AnswerList { get; set; }
        public virtual IList<ContentSectionQuestionBackup> QuestionSections { get; set; }

    }
    public class ContentSectionCorrectAnswerBackup : ContentSectionCorrectAnswer
    {
        public ContentSectionCorrectAnswerBackup() { }
        public ContentSectionCorrectAnswerBackup(ContentSectionCorrectAnswer contentSectionCorrectAnswer)
        {

            Id = contentSectionCorrectAnswer.Id;
            QuestionModelId = contentSectionCorrectAnswer.QuestionModelId;
            Value = contentSectionCorrectAnswer.Value;
            Type = contentSectionCorrectAnswer.Type;
            IsActive = contentSectionCorrectAnswer.IsActive;
            CreatedBy = contentSectionCorrectAnswer.CreatedBy;
            ModifiedBy = contentSectionCorrectAnswer.ModifiedBy;
            DateAdded = contentSectionCorrectAnswer.DateAdded;
            DateModified = contentSectionCorrectAnswer.DateModified;
        }
    }
    public class AnswerModelBackup : AnswerModel
    {
        public AnswerModelBackup() { }
        public AnswerModelBackup(AnswerModel answerModel)
        {
            Id = answerModel.Id;
            AnswerType = answerModel.AnswerType;
            WrongAnswers = answerModel.WrongAnswers?.Select(x => new ContentSectionAnswerBackup(x)).ToList();
            DifficultyLevel = answerModel.DifficultyLevel;
            QuestionModelId = answerModel.QuestionModelId;
            IsActive = answerModel.IsActive;
            CreatedBy = answerModel.CreatedBy;
            ModifiedBy = answerModel.ModifiedBy;
            DateAdded = answerModel.DateAdded;
            DateModified = answerModel.DateModified;
        }
#pragma warning disable CS0114 // Member hides inherited member; missing override keyword
        public virtual IList<ContentSectionAnswerBackup> WrongAnswers { get; set; }
#pragma warning restore CS0114 // Member hides inherited member; missing override keyword

        internal AnswerModel ToAnswerList()
        {
            return new AnswerModel()
            {
                Id = this.Id,
                AnswerType = this.AnswerType,
                WrongAnswers = this.WrongAnswers?.Select(x => x as ContentSectionAnswer).ToList(),
                DifficultyLevel = this.DifficultyLevel,
                QuestionModelId = this.QuestionModelId,
                IsActive = this.IsActive,
                CreatedBy = this.CreatedBy,
                ModifiedBy = this.ModifiedBy,
                DateAdded = this.DateAdded,
                DateModified = this.DateModified
            };
        }
    }
    public class ContentSectionAnswerBackup : ContentSectionAnswer
    {
        public ContentSectionAnswerBackup() { }
        public ContentSectionAnswerBackup(ContentSectionAnswer contentSectionAnswer)
        {
            Id = contentSectionAnswer.Id;
            AnswerModelId = contentSectionAnswer.AnswerModelId;
            Value = contentSectionAnswer.Value;
            Type = contentSectionAnswer.Type;
            IsActive = contentSectionAnswer.IsActive;
            CreatedBy = contentSectionAnswer.CreatedBy;
            ModifiedBy = contentSectionAnswer.ModifiedBy;
            DateAdded = contentSectionAnswer.DateAdded;
            DateModified = contentSectionAnswer.DateModified;
        }
    }
    public class ContentSectionQuestionBackup : ContentSectionQuestion
    {
        public ContentSectionQuestionBackup() { }
        public ContentSectionQuestionBackup(ContentSectionQuestion contentSectionQuestion)
        {
            Id = contentSectionQuestion.Id;
            Value = contentSectionQuestion.Value;
            Type = contentSectionQuestion.Type;
            QuestionModelId = contentSectionQuestion.QuestionModelId;
            FileModelId = contentSectionQuestion.FileModelId;
            IsActive = contentSectionQuestion.IsActive;
            CreatedBy = contentSectionQuestion.CreatedBy;
            ModifiedBy = contentSectionQuestion.ModifiedBy;
            DateAdded = contentSectionQuestion.DateAdded;
            DateModified = contentSectionQuestion.DateModified;
        }
    }

}