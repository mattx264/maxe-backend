﻿using POL.Maxe.Entity.ExamRunModels;
using System;

namespace POL.Maxe.WebService.ViewModels
{
    public class ExamRunViewModel
    {
        public ExamRunViewModel(ExamRunModel examRun)
        {
            Guid = examRun.Guid;
            QuestionCount = examRun.RunQuestionModels.Count;
            //CurrentQuestionId = examRun.RunQuestionModels[examRun.CurrentQuestionIndex].QuestionId;
            CurrentQuestionIndex = examRun.CurrentQuestionIndex;
            QuestionRepeat = examRun.QuestionRepeat;
            Language = examRun.Language;
        }

        public Guid Guid { get; }
        public int QuestionCount { get; }
        // public int CurrentQuestionId { get; set; }
        public int CurrentQuestionIndex { get; }
        public int? QuestionRepeat { get; }
        public string Language { get; }
    }
}
