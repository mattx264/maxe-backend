﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.ViewModels.Helpers;

namespace POL.Maxe.WebService.ViewModels
{
    public class ExamSearchViewModel
    {

        public ExamSearchViewModel(ExamModel examModel, HttpRequest request)
        {
            Title = examModel.Title;
            if (examModel.FileModel != null)
            {
                ImagePath = $"{FileExtension.GetBaseUrl(request)}/Uploads/{examModel.FileModel.FilePath}";
            }
            Id = examModel.Id;
            QuestionCount = examModel.QuestionModels.Count;
        }

        public string Title { get; }
        public string ImagePath { get; }
        public int Id { get; }
        public int QuestionCount { get; set; }
    }
}
