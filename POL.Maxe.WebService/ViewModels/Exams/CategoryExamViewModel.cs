﻿using POL.Maxe.Entity.ExamModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.ViewModels.Exams
{
    public class CategoryExamViewModel
    {
        public CategoryExamViewModel(CategoryExam x)
        {
            Id = x.Id;
            Name = x.Name;
            ParentCategoryExamId = x.ParentCategoryExamId;
        }

        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? ParentCategoryExamId { get; set; }
    }
}
