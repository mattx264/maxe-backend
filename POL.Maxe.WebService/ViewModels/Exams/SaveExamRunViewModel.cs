﻿using POL.Maxe.Entity.Enums;

namespace POL.Maxe.WebService.ViewModels
{
    public class SaveExamRunViewModel
    {
        public int[] ExamIds { get; set; }
        public int? QuestionRepeat { get; set; }
        public bool RandomizeQuestions { get; set; }
        public int? QuestionLimit { get; set; }//how many question 
        public ExamTypeEnum ExamType { get; set; }

    }
}
