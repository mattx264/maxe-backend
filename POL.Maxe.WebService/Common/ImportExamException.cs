﻿using System;

namespace POL.Maxe.WebService.Common
{
    public class ImportExamException : Exception
    {
        public ImportExamException(string message) : base(message)
        {

        }
    }
}
