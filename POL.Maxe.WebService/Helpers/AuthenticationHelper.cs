﻿using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.User;
using POL.Maxe.WebService.Helpers.Interfaces;
using System.Security.Claims;

namespace POL.Maxe.WebService.Helpers
{
    public class AuthenticationHelper : IAuthenticationHelper
    {
        private readonly IUserModelRepository _userRepository;

        public AuthenticationHelper(IUserModelRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public UserModel GetUser(ClaimsPrincipal user)
        {
            string guid = user.FindFirst("Guid")?.Value;
            if (guid == null)
            {
                return null;
            }
            UserModel userModel = _userRepository.GetByGuid(guid);
            return userModel;
        }
    }
}
