﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Helpers.Interfaces
{
    public interface IFileHelper
    {
        public Task<FileModel> SaveFileAsync(IFormFile file);
    }
}
