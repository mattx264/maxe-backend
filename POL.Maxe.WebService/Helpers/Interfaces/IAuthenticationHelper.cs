﻿using POL.Maxe.Entity.User;
using System.Security.Claims;

namespace POL.Maxe.WebService.Helpers.Interfaces
{
    public interface IAuthenticationHelper
    {
        public UserModel GetUser(ClaimsPrincipal user);
    }
}
