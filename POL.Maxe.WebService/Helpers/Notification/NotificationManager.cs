﻿namespace POL.Maxe.WebService.Helpers.Notification
{
    public class NotificationManager : INotificationManager
    {
        private IEmailNotification _emailNotification;
        private ISmsNotification _smsNotification;

        public NotificationManager(IEmailNotification emailNotification, ISmsNotification smsNotification)
        {
            _emailNotification = emailNotification;
            _smsNotification = smsNotification;
        }
        public void SendEmail(string emailAddress)
        {
            _emailNotification.Send(emailAddress);
        }

        public void SendSMS(string phoneNumber)
        {
            _smsNotification.SendSms(phoneNumber);
        }
    }
    public interface INotificationManager
    {
        void SendEmail(string emailAddress);
        void SendSMS(string phoneNumber);
    }
}
