﻿using System;

namespace POL.Maxe.WebService.Helpers
{
    public class Clock : IClock
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }

        public DateTime GetDateTimeUtcNow()
        {
            return DateTime.UtcNow;
        }
    }
    public interface IClock
    {
        DateTime GetDateTimeNow();
        DateTime GetDateTimeUtcNow();
    }
}
