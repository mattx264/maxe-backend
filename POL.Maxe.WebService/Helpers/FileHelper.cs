﻿using Microsoft.AspNetCore.Http;
using POL.Maxe.Entity.User;
using POL.Maxe.WebService.Helpers.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;

namespace POL.Maxe.WebService.Helpers
{
    public class FileHelper : IFileHelper
    {
        public async Task<FileModel> SaveFileAsync(IFormFile file)
        {
            var fileModel = new FileModel();
            var path = Path.Combine(Environment.CurrentDirectory, "wwwroot","Uploads");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //add date folder
            path = Path.Combine(path, DateTime.Now.ToString("ddMMyyyy"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            fileModel.FileName = Path.GetFileName(file.FileName);

            fileModel.Extension = Path.GetExtension(file.FileName);
            var guid = Guid.NewGuid().ToString();
            fileModel.GuidName = guid;
            var fileName = guid + fileModel.Extension; 
            var filePath = Path.Combine(path, fileName); 
            fileModel.FilePath = Path.Combine(DateTime.Now.ToString("ddMMyyyy"), fileName);
            using (FileStream stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return fileModel;
        }
    }
}
