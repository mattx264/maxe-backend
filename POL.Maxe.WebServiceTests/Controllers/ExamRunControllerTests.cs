﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceTests.Controllers
{
    [TestFixture]
    class ExamRunControllerTests
    {
        private ExamRunController _controller;
        private ServiceProvider serviceProvider { get; set; }

        private DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }
        [SetUp]
        public void Setup()
        {
            var services = new ServiceCollection();

            services.AddDbContext<MaxeDbContext>(options =>
               //  options.UseInMemoryDatabase("inmemory")
               options.UseSqlite(CreateInMemoryDatabase())
            );


            services.AddTransient<IExamRunModelRepository, ExamRunRepository>();
            services.AddTransient<IExamModelRepository, ExamModelRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ExamRunController>();

            serviceProvider = services.BuildServiceProvider();
            var context = serviceProvider.GetRequiredService<MaxeDbContext>();
            context.Database.EnsureCreated();

            _controller = serviceProvider.GetService<ExamRunController>();

            var options = new DbContextOptionsBuilder<MaxeDbContext>()
               .UseInMemoryDatabase(databaseName: "Test")
               .Options;
           
        }

        [Test]
        public async Task StartAsync()
        {
            var result = await _controller.StartAsync(new SaveExamRunViewModel()
            {
                ExamIds = new int[] { 1 },
                QuestionLimit = null,
                QuestionRepeat = null,
                RandomizeQuestions = false

            });
            
            var resultFromGet = _controller.Get(result);
            var examRunViewModel = resultFromGet as ExamRunViewModel;
            Assert.AreEqual(examRunViewModel.Guid, new Guid(result.ToString()));
        }
    }
}
