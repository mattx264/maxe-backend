﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.DatabaseContext;
using POL.Maxe.DataAccess.Repositories;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.DataAccess.SeedData;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceTests.Controllers
{
    [TestFixture]
    class BackupControllerTests
    {

        [Test]
        public async Task FullTest()
        {
            var controller = GetInstance();
            var backup = controller.Get();
            var returnCount = await controller.PostAsync(backup);
            Assert.AreEqual(backup.ExamBackupViewModels.Count, returnCount);
        }

        private BackupController GetInstance()
        {
            var examModelRepository = new Mock<IExamModelRepository>();
            var _mockedEntityQuery = new List<ExamModel>()
            {
                ExamModelSeed.GetTestExamModel()
            };
            examModelRepository.Setup(x => x.FindAll()).Returns(_mockedEntityQuery.AsQueryable());
            var unitOfWork = new Mock<IUnitOfWork>();


            var backupController = new BackupController(examModelRepository.Object,
                unitOfWork.Object);
            var ctx = new ControllerContext() { HttpContext = new DefaultHttpContext() };
            backupController.ControllerContext = ctx;
            return backupController;

        }
    }

}
