﻿using Moq;
using NUnit.Framework;
using POL.Maxe.DataAccess;
using POL.Maxe.DataAccess.Repositories.Interfaces;
using POL.Maxe.Entity.ExamModels;
using POL.Maxe.WebService.Controllers;
using POL.Maxe.WebService.Controllers.Builder;
using POL.Maxe.WebService.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace POL.Maxe.WebServiceTests.Controllers
{
    [TestFixture]
    class FileExamBuilderControllerTests
    {
        [Test]
        public void ProcessFile()
        {
            var controller = GetInstance();
            var examModel = controller.ProcessCsvFile(@"
info|Simple Test|Programing|description
question|TEXT|On what planet we live on ?|
correctAnswer|TEXT|Earth|
wrongAnswer|TEXT|Mars|
wrongAnswer|TEXT|Venus|
wrongAnswer|TEXT|Sun|
answer|QUIZ|Normal
answer|TEXT|Hard
");
            Assert.AreEqual(examModel.QuestionModels.Count, 1);
            var questionModel = examModel.QuestionModels.First();
            Assert.NotNull(questionModel.ContentSectionCorrectAnswers);
            Assert.AreEqual(questionModel.AnswerList.Count, 2);

        }

        [Test]
        public void ProcessFile_TwoQuestions()
        {
            var controller = GetInstance();
            var examModel = controller.ProcessCsvFile(@"
info|Simple Test|Programing|description
question|TEXT|On what planet we live on ?|
correctAnswer|TEXT|Earth|
wrongAnswer|TEXT|Mars|
wrongAnswer|TEXT|Venus|
wrongAnswer|TEXT|Sun|
answer|QUIZ|Normal
answer|TEXT|Normal
question|TEXT|On what planet we live on ?|
correctAnswer|TEXT|Earth|
wrongAnswer|TEXT|Mars|
wrongAnswer|TEXT|Venus|
wrongAnswer|TEXT|Sun|
answer|QUIZ|Normal
answer|TEXT|Normal
");
            Assert.AreEqual(examModel.QuestionModels.Count, 2);
            var questionModel = examModel.QuestionModels.Last();
            Assert.NotNull(questionModel.ContentSectionCorrectAnswers);
            Assert.AreEqual(questionModel.AnswerList.Count, 2);

        }
        [Test]
        public void ProcessFile_ManyQuestionSection()
        {
            var controller = GetInstance();
            var examModel = controller.ProcessCsvFile(@"
info|Simple Test|Programing|description
question|TEXT|What is on image?|
question|IMAGE||123444
correctAnswer|TEXT|Earth|
answer|QUIZ|Normal
answer|TEXT|Hard
");
            Assert.AreEqual(examModel.QuestionModels.Count, 1);
            var questionModel = examModel.QuestionModels.Last();
            Assert.NotNull(questionModel.ContentSectionCorrectAnswers);
            Assert.AreEqual(questionModel.QuestionSections.Count, 2);

        }

        private FileExamBuilderController GetInstance()
        {
            var categoryExamRepository = new Mock<ICategoryExamRepository>();
            var _mockedEntityQuery = new List<CategoryExam>()
            {
                new CategoryExam()
                {
                    Name="Programing"
                }
            };

            categoryExamRepository.Setup(x => x.FindByCondition(It.IsAny<Expression<Func<CategoryExam, bool>>>())).Returns(
                _mockedEntityQuery.AsQueryable());

            return new FileExamBuilderController(new Mock<IExamModelRepository>().Object,
                categoryExamRepository.Object,
                new Mock<IAuthenticationHelper>().Object,
                new Mock<IUnitOfWork>().Object);

        }
    }
}
